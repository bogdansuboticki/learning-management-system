package rs.novisad.singidunum.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.novisad.singidunum.model.EvaluacijaZnanja;

@Repository
public interface EvaluacijaZnanjaRepository extends CrudRepository<EvaluacijaZnanja, Long> {
}
