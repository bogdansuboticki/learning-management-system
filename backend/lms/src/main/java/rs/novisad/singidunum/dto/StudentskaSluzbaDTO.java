package rs.novisad.singidunum.dto;

public class StudentskaSluzbaDTO extends KorisnikDTO {

    public StudentskaSluzbaDTO()
    {

    }

    public StudentskaSluzbaDTO(Long id, String korisnickoIme, String email)
    {
        super(korisnickoIme, email);
    }
}

