package rs.novisad.singidunum.service;



import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import rs.novisad.singidunum.model.Polaganje;
import rs.novisad.singidunum.repository.PolaganjeRepository;

@Service
public class PolaganjeService {
    @Autowired
    private PolaganjeRepository polaganjeRepository;

    public List<Polaganje> findByEvaluacijaZnanjaRealizacijaPredmetaPredmetIdAndStudentId(Long pohadjanjeId, Long studentNaGodiniId){
        return polaganjeRepository.findByEvaluacijaZnanjaRealizacijaPredmetaPredmetIdAndStudentNaGodiniId(pohadjanjeId, studentNaGodiniId);
    }

    public Iterable<Polaganje> findAll() {
        return polaganjeRepository.findAll();
    }

    public Optional<Polaganje> findOne(Long id) {
        return polaganjeRepository.findById(id);
    }

    public void save(Polaganje polaganje) {
        polaganjeRepository.save(polaganje);
    }

    public void delete(Long id) {
        polaganjeRepository.deleteById(id);
    }

    public void delete(Polaganje polaganje) {
        polaganjeRepository.delete(polaganje);
    }

}
