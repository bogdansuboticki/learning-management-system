package rs.novisad.singidunum.model;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;

@Entity
public class Student extends Korisnik{
private String ime;
@Column(unique = true)
private String jmbg;

@OneToOne(mappedBy = "student")
private StudentNaGodini studentNaGodini;

@ManyToMany(mappedBy = "studenti")
private List<PohadjanjePredmeta> pohadjanjePredmeta;

@OneToMany(mappedBy = "student")
private List<PrijavljenIspit> prijavljenIspit;



    public List<PohadjanjePredmeta> getPohadjanjePredmeta() {
    return pohadjanjePredmeta;
}

public void setPohadjanjePredmetaList(List<PohadjanjePredmeta> pohadjanjePredmetaList) {
    this.pohadjanjePredmeta = pohadjanjePredmetaList;
}

    public Student() {
        super();
    }

    public Student(String username, String email, String password, String ime, String jmbg) {
        super(username, email, password);
        this.ime = ime;
        this.jmbg = jmbg;
    }

    

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getJmbg() {
        return jmbg;
    }

    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }

    public StudentNaGodini getStudentNaGodini() {
        return studentNaGodini;
    }

    public void setStudentNaGodini(StudentNaGodini studentNaGodini) {
        this.studentNaGodini = studentNaGodini;
    }

    @Override
    public String toString() {
        return "Student [ime=" + ime + ", jmbg=" + jmbg + ", studentNaGodini=" + studentNaGodini + "]";
    }

    public void setPohadjanjePredmeta(List<PohadjanjePredmeta> pohadjanjePredmeta) {
        this.pohadjanjePredmeta = pohadjanjePredmeta;
    }

    public List<PrijavljenIspit> getPrijavljenIspit() {
        return prijavljenIspit;
    }

    public void setPrijavljenIspit(List<PrijavljenIspit> prijavljenIspit) {
        this.prijavljenIspit = prijavljenIspit;
    }



    
}

