package rs.novisad.singidunum.dto;



public class PohadjanjePredmetaDTO {
private Long id;
private Long konacnaOcena;

    public PohadjanjePredmetaDTO() {
        super();
    }

    public PohadjanjePredmetaDTO(Long id, Long konacnaOcena) {
        this.id = id;
        this.konacnaOcena = konacnaOcena;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getKonacnaocena() {
        return konacnaOcena;
    }

    public void setKonacnaocena(Long konacnaOcena) {
        this.konacnaOcena = konacnaOcena;
    }

}

