package rs.novisad.singidunum.repository;

import org.springframework.data.repository.CrudRepository;

import rs.novisad.singidunum.model.StudentNaGodini;
import org.springframework.stereotype.Repository;
@Repository
public interface StudentNaGodiniRepository extends CrudRepository<StudentNaGodini, Long>{

}
