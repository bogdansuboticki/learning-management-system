package rs.novisad.singidunum.dto;

import java.sql.Date;
import java.util.List;

public class UniverzitetDTO {

    private Long id;
    private Date datumOsnivanja;
    private String naziv;
    private List<Long> fakultetiIds;

    public UniverzitetDTO() {
        // Default constructor
    }

    public UniverzitetDTO(Long id, Date datumOsnivanja, String naziv, List<Long> fakultetiIds) {
        this.id = id;
        this.datumOsnivanja = datumOsnivanja;
        this.naziv = naziv;
        this.fakultetiIds = fakultetiIds;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDatumOsnivanja() {
        return datumOsnivanja;
    }

    public void setDatumOsnivanja(Date datumOsnivanja) {
        this.datumOsnivanja = datumOsnivanja;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public List<Long> getFakultetiIds() {
        return fakultetiIds;
    }

    public void setFakultetiIds(List<Long> fakultetiIds) {
        this.fakultetiIds = fakultetiIds;
    }

    @Override
    public String toString() {
        return "UniverzitetDTO [id=" + id + ", datumOsnivanja=" + datumOsnivanja + ", naziv=" + naziv
                + ", fakultetiIds=" + fakultetiIds + "]";
    }

}
