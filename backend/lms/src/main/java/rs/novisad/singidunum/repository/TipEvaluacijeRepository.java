package rs.novisad.singidunum.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.novisad.singidunum.model.TipEvaluacije;

@Repository
public interface TipEvaluacijeRepository extends CrudRepository<TipEvaluacije, Long> {
}
