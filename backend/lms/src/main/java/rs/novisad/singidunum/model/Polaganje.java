package rs.novisad.singidunum.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;

@Entity
public class Polaganje {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    Long bodovi;
    String napomena;

    @OneToOne
    private EvaluacijaZnanja evaluacijaZnanja;

    @ManyToOne
    private StudentNaGodini studentNaGodini;

    

    public Polaganje() {
    }

    public Polaganje(Long id, Long bodovi, String napomena) {
        this.id = id;
        this.bodovi = bodovi;
        this.napomena = napomena;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBodovi() {
        return bodovi;
    }

    public void setBodovi(Long bodovi) {
        this.bodovi = bodovi;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }

    public EvaluacijaZnanja getEvaluacijaZnanja() {
        return evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja(EvaluacijaZnanja evaluacijaZnanja) {
        this.evaluacijaZnanja = evaluacijaZnanja;
    }

    public StudentNaGodini getStudentNaGodini() {
        return studentNaGodini;
    }

    public void setStudentNaGodini(StudentNaGodini studentNaGodini) {
        this.studentNaGodini = studentNaGodini;
    }


    
}
