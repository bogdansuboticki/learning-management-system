package rs.novisad.singidunum.dto;


public class RealizacijaPredmetaDTO {

private Long id;

    public RealizacijaPredmetaDTO() {
        super();
    }

    public RealizacijaPredmetaDTO(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
