package rs.novisad.singidunum.model;

import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;

@Entity
public class RealizacijaPredmeta {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @OneToOne
    private Predmet predmet;

    

    @OneToOne(mappedBy = "realizacijaPredmeta")
    private PohadjanjePredmeta pohadjanjePredmeta;

    @OneToMany(mappedBy = "realizacijaPredmeta")
    private List<EvaluacijaZnanja> evaluacijaZnanja;

    @OneToOne
    private NastavnikNaRealizaciji nastavnikNaRealizaciji;


    @OneToOne(mappedBy = "realizacijaPredmeta")
    private TerminNastave terminNastave;


    

    public List<EvaluacijaZnanja> getEvaluacijaZnanja() {
        return evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja(List<EvaluacijaZnanja> evaluacijaZnanja) {
        this.evaluacijaZnanja = evaluacijaZnanja;
    }

    public RealizacijaPredmeta() {
    }

    public RealizacijaPredmeta(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Predmet getPredmet() {
        return predmet;
    }

    public void setPredmet(Predmet predmet) {
        this.predmet = predmet;
    }

    public PohadjanjePredmeta getPohadjanjePredmeta() {
        return pohadjanjePredmeta;
    }

    public void setPohadjanjePredmeta(PohadjanjePredmeta pohadjanjePredmeta) {
        this.pohadjanjePredmeta = pohadjanjePredmeta;
    }

    public NastavnikNaRealizaciji getNastavnikNaRealizaciji() {
        return nastavnikNaRealizaciji;
    }

    public void setNastavnikNaRealizaciji(NastavnikNaRealizaciji nastavnikNaRealizaciji) {
        this.nastavnikNaRealizaciji = nastavnikNaRealizaciji;
    }

    public TerminNastave getTerminNastave() {
        return terminNastave;
    }

    public void setTerminNastave(TerminNastave terminNastave) {
        this.terminNastave = terminNastave;
    }


}
