package rs.novisad.singidunum.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import rs.novisad.singidunum.model.Predmet;

public class PolozeniIspitiDTO {
  
    @JsonProperty("espb")
    private Long espb;
    
    @JsonProperty("bodovi")
    private Long bodovi;
    
    @JsonProperty("konacnaOcena")
    private Long konacnaOcena;

    private String nazivPredmeta;





    public PolozeniIspitiDTO() {
    }



    public PolozeniIspitiDTO(Long espb, Long bodovi, Long konacnaOcena, String nazivPredmeta) {
        this.bodovi = bodovi;
        this.espb = espb;
        this.konacnaOcena = konacnaOcena;
        this.nazivPredmeta = nazivPredmeta;

    
    }







    public Long getEspb() {
        return espb;
    }



    public void setEspb(Long espb) {
        this.espb = espb;
    }



    public Long getKonacnaOcena() {
        return konacnaOcena;
    }



    public void setKonacnaOcena(Long konacnaOcena) {
        this.konacnaOcena = konacnaOcena;
    }




    public Long getBodovi() {
        return bodovi;
    }



    public void setBodovi(Long bodovi) {
        this.bodovi = bodovi;
    }




    public String getNazivPredmeta() {
        return nazivPredmeta;
    }



    public void setNazivPredmeta(String nazivPredmeta) {
        this.nazivPredmeta = nazivPredmeta;
    }

}
