package rs.novisad.singidunum.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.model.Univerzitet;
import rs.novisad.singidunum.repository.UniverzitetRepository;

@Service
public class UniverzitetService {
    @Autowired
    private UniverzitetRepository univerzitetRepository;

    public Iterable<Univerzitet> findAll() {
        return univerzitetRepository.findAll();
    }

    public Optional<Univerzitet> findOne(Long id) {
        return univerzitetRepository.findById(id);
    }

    public void save(Univerzitet univerzitet) {
        univerzitetRepository.save(univerzitet);
    }

    public void delete(Long id) {
        univerzitetRepository.deleteById(id);
    }

    public void delete(Univerzitet univerzitet) {
        univerzitetRepository.delete(univerzitet);
    }
}
