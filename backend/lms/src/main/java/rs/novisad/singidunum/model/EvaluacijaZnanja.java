package rs.novisad.singidunum.model;

import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;

@Entity
public class EvaluacijaZnanja {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    LocalDateTime vremePocetka;
    LocalDateTime vremeZavrsetka;
    Long bodovi;

    @ManyToOne
    private RealizacijaPredmeta realizacijaPredmeta;

    @ManyToOne
    private TipEvaluacije tipEvaluacije;

    @OneToOne(mappedBy = "evaluacijaZnanja")
    private Polaganje polaganje;

    @OneToOne
    private PrijavljenIspit prijavljenIspit;



    
    public EvaluacijaZnanja() {
    }

    public EvaluacijaZnanja(Long id, LocalDateTime vremePocetka, LocalDateTime vremeZavrsetka, Long bodovi) {
        this.id = id;
        this.vremePocetka = vremePocetka;
        this.vremeZavrsetka = vremeZavrsetka;
        this.bodovi = bodovi;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getVremePocetka() {
        return vremePocetka;
    }

    public void setVremePocetka(LocalDateTime vremePocetka) {
        this.vremePocetka = vremePocetka;
    }

    public LocalDateTime getVremeZavrsetka() {
        return vremeZavrsetka;
    }

    public void setVremeZavrsetka(LocalDateTime vremeZavrsetka) {
        this.vremeZavrsetka = vremeZavrsetka;
    }

    public Long getBodovi() {
        return bodovi;
    }

    public void setBodovi(Long bodovi) {
        this.bodovi = bodovi;
    }

    public RealizacijaPredmeta getRealizacijaPredmeta() {
        return realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
        this.realizacijaPredmeta = realizacijaPredmeta;
    }

    public TipEvaluacije getTipEvaluacije() {
        return tipEvaluacije;
    }

    public void setTipEvaluacije(TipEvaluacije tipEvaluacije) {
        this.tipEvaluacije = tipEvaluacije;
    }

    public Polaganje getPolaganje() {
        return polaganje;
    }

    public void setPolaganje(Polaganje polaganje) {
        this.polaganje = polaganje;
    }

    public PrijavljenIspit getPrijavljenIspit() {
        return prijavljenIspit;
    }

    public void setPrijavljenIspit(PrijavljenIspit prijavljenIspit) {
        this.prijavljenIspit = prijavljenIspit;
    }


    
}
