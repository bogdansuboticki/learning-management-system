package rs.novisad.singidunum.service;



import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.model.EvaluacijaZnanja;
import rs.novisad.singidunum.repository.EvaluacijaZnanjaRepository;

@Service
public class EvaluacijaZnanjaService {
    @Autowired
    private EvaluacijaZnanjaRepository evaluacijaznanjaRepository;

    public Iterable<EvaluacijaZnanja> findAll() {
        return evaluacijaznanjaRepository.findAll();
    }

    public Optional<EvaluacijaZnanja> findOne(Long id) {
        return evaluacijaznanjaRepository.findById(id);
    }

    public void save(EvaluacijaZnanja evaluacijaznanja) {
        evaluacijaznanjaRepository.save(evaluacijaznanja);
    }

    public void delete(Long id) {
        evaluacijaznanjaRepository.deleteById(id);
    }

    public void delete(EvaluacijaZnanja evaluacijaznanja) {
        evaluacijaznanjaRepository.delete(evaluacijaznanja);
    }

}
