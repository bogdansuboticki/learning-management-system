package rs.novisad.singidunum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rs.novisad.singidunum.model.Korisnik;
import rs.novisad.singidunum.service.UserDetailsServiceImpl;
import rs.novisad.singidunum.utils.TokenUtils;

@RestController
@RequestMapping("/api")
public class LoginController {
    @Autowired
    PasswordEncoder passwordEncoder;
    
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    TokenUtils tokenUtils;

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @RequestMapping(path="/login", method = RequestMethod.POST)
    public ResponseEntity<String> login(@RequestBody Korisnik korisnik) {
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(korisnik.getUsername(), korisnik.getPassword());
            System.out.println("Authentication token: " + token);
    
            Authentication auth = authenticationManager.authenticate(token);
    
            SecurityContextHolder.getContext().setAuthentication(auth);
            String jwt = tokenUtils.generateToken(userDetailsService.loadUserByUsername(korisnik.getUsername()));
            System.out.println("JWT token: " + jwt);
    
            return new ResponseEntity<String>(jwt, HttpStatus.OK);
        } catch (AuthenticationException e) {
            System.out.println("Authentication failed: " + e.getMessage());
            return new ResponseEntity<String>("Authentication failed", HttpStatus.UNAUTHORIZED);
        } catch (Exception e) {
            System.out.println("Error during authentication: " + e.getMessage());
            return new ResponseEntity<String>("Error during authentication", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
