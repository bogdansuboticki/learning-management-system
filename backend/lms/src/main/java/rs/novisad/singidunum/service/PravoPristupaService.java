package rs.novisad.singidunum.service;

import java.util.Optional;

import javax.swing.text.html.Option;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.model.PravoPristupa;
import rs.novisad.singidunum.repository.PravoPristupaRepository;

@Service
public class PravoPristupaService {
    @Autowired
    private PravoPristupaRepository pravoPristupaRepository;

    public Iterable<PravoPristupa> findAll() {
        return pravoPristupaRepository.findAll();
    }

    public Optional<PravoPristupa> findByNaziv(String naziv){
        return pravoPristupaRepository.findByNaziv(naziv);
    }

    public Optional<PravoPristupa> findOne(Long id) {
        return pravoPristupaRepository.findById(id);
    }

    public Optional<PravoPristupa> findByVlasnikId(Long id, Long korisnikId){
        return pravoPristupaRepository.findByIdAndKorisniciId(id, korisnikId);
    }

    public void save(PravoPristupa pravoPristupa) {
        pravoPristupaRepository.save(pravoPristupa);
    }

    public void delete(Long id) {
        pravoPristupaRepository.deleteById(id);
    }

    public void delete(PravoPristupa pravoPristupa) {
        pravoPristupaRepository.delete(pravoPristupa);
    }
}
