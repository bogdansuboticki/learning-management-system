package rs.novisad.singidunum.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.persistence.EntityNotFoundException;
import rs.novisad.singidunum.dto.PrijavljenIspitDTO;
import rs.novisad.singidunum.service.PrijavljenIspitService;

@Controller
@RequestMapping(path = "/api/studenti")
public class PrijavljenIspitController {
    @Autowired
    PrijavljenIspitService prijavljenIspitService;

       @PostMapping("/prijavi-ispit")
    public ResponseEntity<?> signUpForExam(@RequestParam Long evaluacijaZnanjaId, @RequestParam Long predmetId, @RequestParam String username) {
        System.out.println("Ispit id:" + evaluacijaZnanjaId);
        try {
            prijavljenIspitService.prijaviIspit(evaluacijaZnanjaId, predmetId, username);
            return ResponseEntity.ok("Uspesno ste prijavili ispit.");
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An error occurred.");
        }
    }


    @GetMapping("/{korisnickoIme}/prijavljeni-ispiti")
    public ResponseEntity<List<PrijavljenIspitDTO>> getStudentExams(@PathVariable String korisnickoIme) {
        List<PrijavljenIspitDTO> prijavljeniIspiti = prijavljenIspitService.getPrijavljeniIspiti(korisnickoIme);
        return ResponseEntity.ok(prijavljeniIspiti);
    }

}
