package rs.novisad.singidunum.repository;



import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.novisad.singidunum.model.Polaganje;

@Repository
public interface PolaganjeRepository extends CrudRepository<Polaganje, Long> {

    List<Polaganje> findByEvaluacijaZnanjaRealizacijaPredmetaPredmetIdAndStudentNaGodiniId(Long pohadjanjeId, Long studentNaGodiniId);
}
