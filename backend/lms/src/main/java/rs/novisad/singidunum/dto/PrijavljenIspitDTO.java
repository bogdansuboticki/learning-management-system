package rs.novisad.singidunum.dto;


public class PrijavljenIspitDTO {

    String nazivPredmeta;
    String nastavnik;
    Long espb;

    boolean prijavljen;


    

    public PrijavljenIspitDTO() {
    }

    
    public PrijavljenIspitDTO(String nazivPredmeta, String nastavnik, Long espb, boolean prijavljen) {
        this.nazivPredmeta = nazivPredmeta;
        this.nastavnik = nastavnik;
        this.espb = espb;
        this.prijavljen = prijavljen;
    }


    public String getNazivPredmeta() {
        return nazivPredmeta;
    }

    public void setNazivPredmeta(String nazivPredmeta) {
        this.nazivPredmeta = nazivPredmeta;
    }

    public String getNastavnik() {
        return nastavnik;
    }

    public void setNastavnik(String nastavnik) {
        this.nastavnik = nastavnik;
    }

    public Long getEspb() {
        return espb;
    }

    public void setEspb(Long espb) {
        this.espb = espb;
    }

    public boolean isPrijavljen() {
        return prijavljen;
    }

    public void setPrijavljen(boolean prijavljen) {
        this.prijavljen = prijavljen;
    }




    
}
