package rs.novisad.singidunum.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.model.GodinaStudija;
import rs.novisad.singidunum.repository.GodinaStudijaRepository;

@Service
public class GodinaStudijaService {
    @Autowired
    private GodinaStudijaRepository godinaStudijaRepository;

    public Iterable<GodinaStudija> findByStudijskiProgramId(Long studijskiProgramId){
        return godinaStudijaRepository.findByStudijskiProgramId(studijskiProgramId);
    }


    public Optional<GodinaStudija> findByStudijskiProgramIdAndGodina(Long studijskiProgramId, Long godina)
    {
        return godinaStudijaRepository.findByStudijskiProgramIdAndGodina(studijskiProgramId, godina);
    }

    public Iterable<GodinaStudija> findAll() {
        return godinaStudijaRepository.findAll();
    }

    public Optional<GodinaStudija> findOne(Long id) {
        return godinaStudijaRepository.findById(id);
    }

    public void save(GodinaStudija godinaStudija) {
        godinaStudijaRepository.save(godinaStudija);
    }

    public void delete(Long id) {
        godinaStudijaRepository.deleteById(id);
    }

    public void delete(GodinaStudija godinaStudija) {
        godinaStudijaRepository.delete(godinaStudija);
    }
}
