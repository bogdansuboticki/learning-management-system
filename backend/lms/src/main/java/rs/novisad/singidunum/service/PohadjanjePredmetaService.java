package rs.novisad.singidunum.service;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.model.PohadjanjePredmeta;
import rs.novisad.singidunum.repository.PohadjanjePredmetaRepository;

@Service
public class PohadjanjePredmetaService {
    @Autowired
    private PohadjanjePredmetaRepository pohadjanjepredmetaRepository;

    public List<PohadjanjePredmeta> findByStudentId(Long studentId) {
        return pohadjanjepredmetaRepository.findByStudentId(studentId);
    }

    public Iterable<PohadjanjePredmeta> findAll() {
        return pohadjanjepredmetaRepository.findAll();
    }

    public Optional<PohadjanjePredmeta> findOne(Long id) {
        return pohadjanjepredmetaRepository.findById(id);
    }

    public void save(PohadjanjePredmeta pohadjanjepredmeta) {
        pohadjanjepredmetaRepository.save(pohadjanjepredmeta);
    }

    public void delete(Long id) {
        pohadjanjepredmetaRepository.deleteById(id);
    }

    public void delete(PohadjanjePredmeta pohadjanjepredmeta) {
        pohadjanjepredmetaRepository.delete(pohadjanjepredmeta);
    }

}
