package rs.novisad.singidunum.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import rs.novisad.singidunum.model.Fakultet;
import org.springframework.stereotype.Repository;
@Repository
public interface FakultetRepository extends CrudRepository<Fakultet, Long>{
    Optional<Fakultet> findByNaziv(String naziv);

}
