package rs.novisad.singidunum.dto;

public class NastavnikDTO extends KorisnikDTO {
    private String ime;
    private String jmbg;
    private String biografija;

    public NastavnikDTO() {
        super();
    }

    public NastavnikDTO(String korisnickoIme, String email, String ime, String jmbg,
            String biografija) {
        super(korisnickoIme, email);
        this.ime = ime;
        this.jmbg = jmbg;
        this.biografija = biografija;
    }

    public NastavnikDTO(Long id, String korisnickoIme, String email, String ime, String jmbg,
            String biografija) {
        super(korisnickoIme, email);
        this.ime = ime;
        this.jmbg = jmbg;
        this.biografija = biografija;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getJmbg() {
        return jmbg;
    }

    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }

    public String getBiografija() {
        return biografija;
    }

    public void setBiografija(String biografija) {
        this.biografija = biografija;
    }


}

