package rs.novisad.singidunum.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.novisad.singidunum.model.Korisnik;
import rs.novisad.singidunum.security.SecurityConfiguration;
import rs.novisad.singidunum.service.KorisnikService;
import rs.novisad.singidunum.dto.RegistrovaniKorisnikDTO;

@Controller
@RequestMapping(path = "/api")
public class RegistracijaController {
    private final KorisnikService korisnikService;
    private final SecurityConfiguration securityConfiguration;

    @Autowired
    public RegistracijaController(KorisnikService korisnikService, SecurityConfiguration securityConfiguration) {
        this.korisnikService = korisnikService;
        this.securityConfiguration = securityConfiguration;
    }

    @PostMapping("/registracija")
    public ResponseEntity<String> register(@RequestBody RegistrovaniKorisnikDTO registrovani) {

        System.out.println("UserType:" + registrovani.toString());
        try{
            String password = securityConfiguration.getPasswordEncoder().encode(registrovani.getPassword());
            Korisnik korisnik = new Korisnik(registrovani.getKorisnickoIme(), registrovani.getEmail(), password);
            korisnikService.save(korisnik);        
        
        }catch (DataIntegrityViolationException e) {
        return ResponseEntity.badRequest().body("Email ili username vec postoji.");
    }

        return ResponseEntity.ok("Korisnik je uspesno registrovan");
    }

}

