package rs.novisad.singidunum.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rs.novisad.singidunum.dto.NastavnikDTO;
import rs.novisad.singidunum.dto.PolozeniIspitiDTO;
import rs.novisad.singidunum.dto.PredmetDTO;
import rs.novisad.singidunum.dto.StudentDTO;
import rs.novisad.singidunum.dto.StudentPodaciDTO;
import rs.novisad.singidunum.model.EvaluacijaZnanja;
import rs.novisad.singidunum.model.GodinaStudija;
import rs.novisad.singidunum.model.Korisnik;
import rs.novisad.singidunum.model.Nastavnik;
import rs.novisad.singidunum.model.NastavnikNaRealizaciji;
import rs.novisad.singidunum.model.PohadjanjePredmeta;
import rs.novisad.singidunum.model.Polaganje;
import rs.novisad.singidunum.model.Predmet;
import rs.novisad.singidunum.model.RealizacijaPredmeta;
import rs.novisad.singidunum.model.Student;
import rs.novisad.singidunum.model.StudentNaGodini;
import rs.novisad.singidunum.model.TipEvaluacije;
import rs.novisad.singidunum.security.SecurityConfiguration;
import rs.novisad.singidunum.service.KorisnikService;
import rs.novisad.singidunum.service.NastavnikNaRealizacijiService;
import rs.novisad.singidunum.service.NastavnikService;
import rs.novisad.singidunum.service.PohadjanjePredmetaService;
import rs.novisad.singidunum.service.PolaganjeService;
import rs.novisad.singidunum.service.PredmetService;
import rs.novisad.singidunum.service.RealizacijaPredmetaService;
import rs.novisad.singidunum.service.StudentService;
import rs.novisad.singidunum.service.TipEvaluacijeService;

@RestController
@RequestMapping("/api/nastavnici")
public class NastavnikController {
    @Autowired
    private NastavnikService nastavnikService;

    @Autowired
    private SecurityConfiguration securityConfiguration;

    @Autowired
    private NastavnikNaRealizacijiService nastavnikNaRealizacijiService;

    @Autowired
    private RealizacijaPredmetaService realizacijaPredmetaService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private PredmetService predmetService;

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private PohadjanjePredmetaService pohadjanjePredmetaService;

    @Autowired
    private PolaganjeService polaganjeService;

    @Autowired
    private TipEvaluacijeService tipEvaluacijeService;

    @GetMapping
    public ResponseEntity<List<NastavnikDTO>> dobaviSveNastavnike(){
        Iterable<Nastavnik> nastavnici = nastavnikService.findAll();
        ArrayList<NastavnikDTO> nastavniciDTO = new ArrayList<>();
        for(Nastavnik nastavnik : nastavnici){
            nastavniciDTO.add(new NastavnikDTO(nastavnik.getUsername(), nastavnik.getEmail(), nastavnik.getIme(), nastavnik.getJmbg(), nastavnik.getBiografija()));
        }
        return new ResponseEntity<>(nastavniciDTO, HttpStatus.OK);
    }

       @GetMapping("/{korisnickoIme}")
    public ResponseEntity<NastavnikDTO> dobaviPoKorisnickomImenu(@PathVariable("korisnickoIme") String korisnickoIme){
        Optional<Nastavnik> nastavnik = nastavnikService.findOne(korisnickoIme);
        if(nastavnik.isPresent()){
            NastavnikDTO nastavnikDTO = new NastavnikDTO(nastavnik.get().getUsername(), nastavnik.get().getEmail() ,nastavnik.get().getIme(), nastavnik.get().getJmbg(), nastavnik.get().getBiografija());
            return new ResponseEntity<>(nastavnikDTO, HttpStatus.OK);
        }
        return new ResponseEntity<NastavnikDTO>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{korisnickoIme}")
    public ResponseEntity<NastavnikDTO> izmeniNastavnika(@PathVariable("korisnickoIme") String korisnickoIme, @RequestBody NastavnikDTO nastavnik) {
        Optional<Nastavnik> ns = nastavnikService.findOne(korisnickoIme);
        if(ns.isPresent() && ns.get().getUsername().equals(nastavnik.getKorisnickoIme())) {
            Nastavnik zaIzmenu = ns.get();
            // zaIzmenu.setPassword(securityConfiguration.getPasswordEncoder().encode(nastavnik.getPassword()));
		    zaIzmenu.setEmail(nastavnik.getEmail());
            zaIzmenu.setBiografija(nastavnik.getBiografija());
            nastavnikService.save(zaIzmenu);
            return new ResponseEntity<NastavnikDTO>(HttpStatus.OK);
    }
    return new ResponseEntity<NastavnikDTO>(HttpStatus.NOT_FOUND);
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody Nastavnik nastavnik) {
        nastavnikService.save(nastavnik);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        nastavnikService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{korisnickoIme}/predmeti")
    public ResponseEntity<List<PredmetDTO>> getCoursesForStudent(@PathVariable String korisnickoIme) {
        ArrayList<PredmetDTO> predmetiDTO = new ArrayList<>();
        Nastavnik nastavnik = nastavnikService.findOne(korisnickoIme).orElse(null);
 
        if (nastavnik == null) {
            return ResponseEntity.notFound().build();
        }

        Long nastavnikID = nastavnik.getId();
        Iterable<NastavnikNaRealizaciji> nastavniciNaRealizaciji = nastavnikNaRealizacijiService.findAll(nastavnikID);

        for(NastavnikNaRealizaciji nastavnikNaRealizaciji: nastavniciNaRealizaciji){
            Long nastavnikNaRealizacijiID = nastavnikNaRealizaciji.getId();
            Optional<RealizacijaPredmeta> realizacijaPredmeta = realizacijaPredmetaService.findOne(nastavnikNaRealizacijiID);
            Predmet predmet = realizacijaPredmeta.get().getPredmet();
            predmetiDTO.add(new PredmetDTO(predmet.getId(), predmet.getNaziv(), predmet.getEspb(), predmet.getObavezan(), predmet.getBrojPredavanja(), predmet.getBrojVezbi(), predmet.getDrugiObliciNastave(), predmet.getIstrazivackiRad(), predmet.getOstaliCasovi()));

        }

       
        return ResponseEntity.ok(predmetiDTO);
    }

    @GetMapping("/studenti-po-predmetu/{nazivPredmeta}")
    public ResponseEntity<List<StudentDTO>> getStudentsByPredmet(@PathVariable String nazivPredmeta) {
    Optional<Predmet> predmetOptional = predmetService.findByNaziv(nazivPredmeta);
    System.out.println("Ovde ide naziv predmeta");
    System.out.println(predmetOptional.get().getNaziv());
    if (!predmetOptional.isPresent()) {
        return ResponseEntity.notFound().build();
    }
 
    Predmet predmet = predmetOptional.get();
    List<Student> students = new ArrayList<>();
 
    GodinaStudija godinaStudija = predmet.getGodinaStudija(); 
    System.out.println(godinaStudija.getGodina());
    for (StudentNaGodini studentNaGodini : godinaStudija.getStudentiNaGodini()) {
            students.add(studentNaGodini.getStudent());
    }
    
    ArrayList<StudentDTO> studentiDTO = new ArrayList<>();
    for(Student student: students){
        System.out.println(student.getIme());
        Korisnik korisnik = korisnikService.findOne(student.getId()).get();
        studentiDTO.add(new StudentDTO(korisnik.getUsername(), korisnik.getEmail(), student.getIme(), student.getJmbg()));
    }

    System.out.println(studentiDTO.get(0).getEmail());
    return ResponseEntity.ok(studentiDTO);

    
}


@GetMapping("/nastavnik-student-podaci/{korisnickoIme}")
public ResponseEntity<StudentPodaciDTO> getStudentsByIme(@PathVariable String korisnickoIme) {
    Optional<Student> studentOptional = studentService.findOne(korisnickoIme);

    if (studentOptional.isEmpty()) {
        return ResponseEntity.notFound().build(); // Student not found
    }

    Student student = studentOptional.get();
    List<Student> students = new ArrayList<>();

    GodinaStudija godinaStudija = student.getStudentNaGodini().getGodinaStudija();
    System.out.println(godinaStudija.getGodina());
    

    Long studentId = student.getId();
    List<PohadjanjePredmeta> pohadjanja = pohadjanjePredmetaService.findByStudentId(studentId);
    List<PolozeniIspitiDTO> passedExams = new ArrayList<>();

    System.out.println("Pohadjanje" + pohadjanja.get(0).getId());

    for (PohadjanjePredmeta pohadjanje : pohadjanja) {
        List<Polaganje> polaganja = polaganjeService.findByEvaluacijaZnanjaRealizacijaPredmetaPredmetIdAndStudentId(pohadjanje.getRealizacijaPredmeta().getPredmet().getId(), student.getStudentNaGodini().getId());

        int passedKolokvijumCount = 0;
        boolean passedIspit = false;
        Long totalBodovi = (long) 0;
        PolozeniIspitiDTO passedExamDto =  null;

        for (Polaganje polaganje : polaganja) {
            System.out.println("Polaganje " +polaganje.getNapomena());
            EvaluacijaZnanja evaluacija = polaganje.getEvaluacijaZnanja();

            System.out.println("Evaluacija " + evaluacija.getBodovi());
            TipEvaluacije tipEvaluacije = evaluacija.getTipEvaluacije();

            System.out.println("tip evaluaicje "  + tipEvaluacije);

            if (tipEvaluacije != null) {
                if (tipEvaluacije.getNaziv().equals("Kolokvijum") && polaganje.getBodovi() >= 15) {
                    System.out.println("Jeste kolokvijum");
                    passedKolokvijumCount++;
                } else if (tipEvaluacije.getNaziv().equals("Ispit") && evaluacija.getPrijavljenIspit() != null) {
                    System.out.println("jeste ispit");
                    passedIspit = true;
                }
                totalBodovi += polaganje.getBodovi();
            }
        }

        if (passedKolokvijumCount >= 2 && passedIspit && totalBodovi >= 51) {
            passedExamDto = new PolozeniIspitiDTO();
            passedExamDto.setNazivPredmeta(pohadjanje.getRealizacijaPredmeta().getPredmet().getNaziv());
            passedExamDto.setBodovi(totalBodovi);
            passedExamDto.setEspb(pohadjanje.getRealizacijaPredmeta().getPredmet().getEspb());
            if(totalBodovi >= 51 && totalBodovi < 61)
            {
                passedExamDto.setKonacnaOcena((long) 6);
            }
            else if(totalBodovi >=61 && totalBodovi < 71)
            {
                passedExamDto.setKonacnaOcena((long) 7);
            }
            else if(totalBodovi >=71 && totalBodovi < 81)
            {
                passedExamDto.setKonacnaOcena((long) 8);
            }
            else if(totalBodovi >=81 && totalBodovi < 91)
            {
                passedExamDto.setKonacnaOcena((long) 9);
            }
            else if(totalBodovi >=91)
            {
                passedExamDto.setKonacnaOcena((long) 10);
            }
            else
            {
                passedExamDto.setKonacnaOcena((long) 5);
            }
            // Add passedExamDto to passedExams list
            passedExams.add(passedExamDto);
        }

    }

    
    StudentPodaciDTO studentPodaciDTO = new StudentPodaciDTO(student.getIme(), student.getJmbg(), student.getStudentNaGodini().getBrojIndeksa(), passedExams);
 
    // System.out.println(studentPodaciDTO.getPolozeni().get(0).getBodovi());

    return ResponseEntity.ok(studentPodaciDTO);
}


}

