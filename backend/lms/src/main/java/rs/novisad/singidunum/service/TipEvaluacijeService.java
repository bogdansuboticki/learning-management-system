package rs.novisad.singidunum.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.model.TipEvaluacije;
import rs.novisad.singidunum.repository.TipEvaluacijeRepository;

@Service
public class TipEvaluacijeService {
    @Autowired
    private TipEvaluacijeRepository tipevaluacijeRepository;

    public Iterable<TipEvaluacije> findAll() {
        return tipevaluacijeRepository.findAll();
    }

    public Optional<TipEvaluacije> findOne(Long id) {
        return tipevaluacijeRepository.findById(id);
    }

    public void save(TipEvaluacije tipevaluacije) {
        tipevaluacijeRepository.save(tipevaluacije);
    }

    public void delete(Long id) {
        tipevaluacijeRepository.deleteById(id);
    }

    public void delete(TipEvaluacije tipevaluacije) {
        tipevaluacijeRepository.delete(tipevaluacije);
    }

}
