package rs.novisad.singidunum.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;


@Entity
public class TipNastave {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String naziv;

    @OneToOne
    private NastavnikNaRealizaciji nastavnikNaRealizaciji;

    @OneToOne(mappedBy = "tipNastave")
    private TerminNastave terminNastave;
}
