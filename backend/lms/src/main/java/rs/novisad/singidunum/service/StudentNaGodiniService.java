package rs.novisad.singidunum.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.model.StudentNaGodini;
import rs.novisad.singidunum.repository.StudentNaGodiniRepository;

@Service
public class StudentNaGodiniService {
    @Autowired
    private StudentNaGodiniRepository studentNaGodiniRepository;

    public Iterable<StudentNaGodini> findAll() {
        return studentNaGodiniRepository.findAll();
    }

    public Optional<StudentNaGodini> findOne(Long id) {
        return studentNaGodiniRepository.findById(id);
    }

    public void save(StudentNaGodini studentNaGodini) {
        studentNaGodiniRepository.save(studentNaGodini);
    }

    public void delete(Long id) {
        studentNaGodiniRepository.deleteById(id);
    }

    public void delete(StudentNaGodini studentNaGodini) {
        studentNaGodiniRepository.delete(studentNaGodini);
    }
}
