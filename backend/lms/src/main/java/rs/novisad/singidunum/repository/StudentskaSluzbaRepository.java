package rs.novisad.singidunum.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.novisad.singidunum.model.StudentskaSluzba;

@Repository
public interface StudentskaSluzbaRepository extends CrudRepository<StudentskaSluzba, Long> {

}

