package rs.novisad.singidunum.model;

import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;

@Entity
public class PohadjanjePredmeta {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    Long konacnaOcena;

    @OneToOne
    private RealizacijaPredmeta realizacijaPredmeta;

    @ManyToMany
    private List<Student> studenti;



    
    public PohadjanjePredmeta() {
    }

    public PohadjanjePredmeta(Long id, Long konacnaOcena) {
        this.id = id;
        this.konacnaOcena = konacnaOcena;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getKonacnaOcena() {
        return konacnaOcena;
    }

    public void setKonacnaOcena(Long konacnaOcena) {
        this.konacnaOcena = konacnaOcena;
    }

    public RealizacijaPredmeta getRealizacijaPredmeta() {
        return realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
        this.realizacijaPredmeta = realizacijaPredmeta;
    }

    
}
