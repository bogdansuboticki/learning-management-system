package rs.novisad.singidunum.dto;

public class RegistrovaniKorisnikDTO {
    private String korisnickoIme;
    private String email;
    private String password;

    public RegistrovaniKorisnikDTO() {
    }


    public RegistrovaniKorisnikDTO(String korisnickoIme, String password, String email) {
        this.korisnickoIme = korisnickoIme;
        this.password = password;
        this.email = email;
    }


    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getEmail() {
        return email;
    }



    public void setEmail(String email) {
        this.email = email;
    }


    

}
