package rs.novisad.singidunum.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import rs.novisad.singidunum.model.PohadjanjePredmeta;
@Repository
public interface PohadjanjePredmetaRepository extends CrudRepository<PohadjanjePredmeta, Long> {
    @Query("SELECT pp FROM PohadjanjePredmeta pp JOIN pp.studenti s WHERE s.id = :studentId")
    List<PohadjanjePredmeta> findByStudentId(@Param("studentId") Long studentId);
}