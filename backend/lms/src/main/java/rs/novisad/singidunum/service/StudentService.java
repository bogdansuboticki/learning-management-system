package rs.novisad.singidunum.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.dto.PolozeniIspitiDTO;
import rs.novisad.singidunum.model.EvaluacijaZnanja;
import rs.novisad.singidunum.model.PohadjanjePredmeta;
import rs.novisad.singidunum.model.Predmet;
import rs.novisad.singidunum.model.Student;
import rs.novisad.singidunum.repository.StudentRepository;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;

    public Iterable<Student> findAll() {
        System.out.println(studentRepository.findAll());
        return studentRepository.findAll();
    }

    public Optional<Student> findOne(Long id) {
        return studentRepository.findById(id);
    }

    public Optional<Student> findOne(String korisnickoIme) {
        return studentRepository.findByUsername(korisnickoIme);
    }

    public void save(Student student) {
        studentRepository.save(student);
    }

    public void delete(Long id) {
        studentRepository.deleteById(id);
    }

    public void delete(Student student) {
        studentRepository.delete(student);
    }


        public List<PolozeniIspitiDTO> getPolozeniIspiti(String korisnickoIme) {
        Optional<Student> studentOptional = studentRepository.findByUsername(korisnickoIme);
        if (studentOptional.isPresent()) {
            Student student = studentOptional.orElse(null);

        List<PolozeniIspitiDTO> polozeniIspiti = new ArrayList<>();
        for (PohadjanjePredmeta pohadjanjePredmeta : student.getPohadjanjePredmeta()) {

            if (pohadjanjePredmeta.getKonacnaOcena() >= 6) {
                Predmet predmet = pohadjanjePredmeta.getRealizacijaPredmeta().getPredmet();
                Long bodovi = getBodoviForStudentAndPredmet(student, predmet);
                
                // polozeniIspiti.add(new PolozeniIspitiDTO(predmet.getNaziv(), predmet.getEspb(), bodovi, pohadjanjePredmeta.getKonacnaOcena()));

            }
        }
        return polozeniIspiti;
        }
        return null;
    }



    public Long getBodoviForStudentAndPredmet(Student student, Predmet predmet) {
    Long totalBodovi = (long) 0.00;
    for (PohadjanjePredmeta pohadjanjePredmeta : student.getPohadjanjePredmeta()) {
        if (pohadjanjePredmeta.getRealizacijaPredmeta().getPredmet().equals(predmet)) {
            for (EvaluacijaZnanja evaluacija : pohadjanjePredmeta.getRealizacijaPredmeta().getEvaluacijaZnanja()) {
                totalBodovi += evaluacija.getBodovi();
            }
        }
    }
    return totalBodovi;
}
}

