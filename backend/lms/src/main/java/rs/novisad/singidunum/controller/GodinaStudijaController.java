package rs.novisad.singidunum.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import rs.novisad.singidunum.dto.GodinaStudijaDTO;
import rs.novisad.singidunum.model.GodinaStudija;
import rs.novisad.singidunum.model.StudijskiProgram;
import rs.novisad.singidunum.service.GodinaStudijaService;
import rs.novisad.singidunum.service.StudijskiProgramService;

@RestController
@RequestMapping("/api/godina-studija")
public class GodinaStudijaController {
    
    @Autowired
    private GodinaStudijaService godinaStudijaService;
    @Autowired
    private StudijskiProgramService studijskiProgramService;

    @GetMapping
    public ResponseEntity<List<GodinaStudijaDTO>> dobaviSveGodineStudija(){
        Iterable<GodinaStudija> godineStudija = godinaStudijaService.findAll();
        ArrayList<GodinaStudijaDTO> godineStudijaDTO = new ArrayList<>();
        for(GodinaStudija godinaStudija : godineStudija){
            godineStudijaDTO.add(new GodinaStudijaDTO(godinaStudija.getGodina()));
        }
        return new ResponseEntity<>(godineStudijaDTO, HttpStatus.OK);
    }

    @GetMapping("/{nazivStudijskogPrograma}")
    public ResponseEntity<List<GodinaStudijaDTO>> dobaviSveGodineStudijaZaSmer(@PathVariable String nazivStudijskogPrograma){
        Optional<StudijskiProgram> studijskiProgram = studijskiProgramService.findByNaziv(nazivStudijskogPrograma);
        
        Iterable<GodinaStudija> godineStudija = studijskiProgram.get().getGodineStudija();
        ArrayList<GodinaStudijaDTO> godineStudijaDTO = new ArrayList<>();
        for(GodinaStudija godinaStudija : godineStudija){
            godineStudijaDTO.add(new GodinaStudijaDTO(godinaStudija.getGodina()));
        }
        return new ResponseEntity<>(godineStudijaDTO, HttpStatus.OK);
    }
}
