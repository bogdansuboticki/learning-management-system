package rs.novisad.singidunum.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.model.StudentskaSluzba;
import rs.novisad.singidunum.repository.StudentskaSluzbaRepository;

@Service
public class StudentskaSluzbaService {
    @Autowired
    private StudentskaSluzbaRepository adminRepository;


    

    public Iterable<StudentskaSluzba> findAll() {
        return adminRepository.findAll();
    }

    public Optional<StudentskaSluzba> findOne(Long id) {
        return adminRepository.findById(id);
    }

    public void save(StudentskaSluzba admin) {
        adminRepository.save(admin);
    }

    public void delete(Long id) {
        adminRepository.deleteById(id);
    }

    public void delete(StudentskaSluzba admin) {
        adminRepository.delete(admin);
    }
}

