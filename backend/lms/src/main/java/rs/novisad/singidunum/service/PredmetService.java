package rs.novisad.singidunum.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.model.Predmet;
import rs.novisad.singidunum.repository.PredmetRepository;

@Service
public class PredmetService {
    @Autowired
    private PredmetRepository predmetRepository;

    public Iterable<Predmet> findAll() {
        return predmetRepository.findAll();
    }

    public Optional<Predmet> findByNaziv(String naziv){
        return predmetRepository.findByNaziv(naziv);
    }

    public Optional<Predmet> findOne(Long id) {
        return predmetRepository.findById(id);
    }

    public void save(Predmet predmet) {
        predmetRepository.save(predmet);
    }

    public void delete(Long id) {
        predmetRepository.deleteById(id);
    }

    public void delete(Predmet predmet) {
        predmetRepository.delete(predmet);
    }
}
