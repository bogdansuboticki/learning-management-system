package rs.novisad.singidunum.model;

import java.time.LocalDateTime;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class GodinaStudija {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long godina;

    @ManyToOne
    private StudijskiProgram studijskiProgram;

    @OneToMany(mappedBy = "godinaStudija")
    private List<StudentNaGodini> studentiNaGodini;

    @OneToMany(mappedBy = "godinaStudija")
    private List<Predmet> predmeti;

    public GodinaStudija() {
        // Default constructor
    }

    
    public GodinaStudija(Long godina, StudijskiProgram studijskiProgram) {
        this.godina = godina;
        this.studijskiProgram = studijskiProgram;
        // Default constructor
    }
    public GodinaStudija(Long godina, StudijskiProgram studijskiProgram, List<StudentNaGodini> studentiNaGodini, List<Predmet> predmeti) {
        this.godina = godina;
        this.studijskiProgram = studijskiProgram;
        this.studentiNaGodini = studentiNaGodini;
        this.predmeti = predmeti;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StudijskiProgram getStudijskiProgram() {
        return studijskiProgram;
    }

    public void setStudijskiProgram(StudijskiProgram studijskiProgram) {
        this.studijskiProgram = studijskiProgram;
    }

    public List<Predmet> getPredmeti() {
        return predmeti;
    }

    public void setPredmeti(List<Predmet> predmeti) {
        this.predmeti = predmeti;
    }

    public List<StudentNaGodini> getStudentiNaGodini() {
        return studentiNaGodini;
    }

    public void setStudentiNaGodini(List<StudentNaGodini> studentiNaGodini) {
        this.studentiNaGodini = studentiNaGodini;
    }

    public Long getGodina() {
        return godina;
    }

    public void setGodina(Long godina) {
        this.godina = godina;
    }
}
