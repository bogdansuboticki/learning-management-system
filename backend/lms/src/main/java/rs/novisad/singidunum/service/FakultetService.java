package rs.novisad.singidunum.service;

import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.model.Fakultet;
import rs.novisad.singidunum.repository.FakultetRepository;

@Service
public class FakultetService {
    @Autowired
    private FakultetRepository fakultetRepository;

    public Iterable<Fakultet> findAll() {
        return fakultetRepository.findAll();
    }

    public Optional<Fakultet> findByNaziv(String naziv)
    {
        return fakultetRepository.findByNaziv(naziv);
    }

    public Optional<Fakultet> findOne(Long id) {
        return fakultetRepository.findById(id);
    }

    public void save(Fakultet fakultet) {
        fakultetRepository.save(fakultet);
    }

    public void delete(Long id) {
        fakultetRepository.deleteById(id);
    }

    public void delete(Fakultet fakultet) {
        fakultetRepository.delete(fakultet);
    }
}
