package rs.novisad.singidunum.dto;

import java.time.LocalDateTime;


public class EvaluacijaZnanjaDTO {
private Long id;
private LocalDateTime vremePocetka;
private LocalDateTime vremeZavrsetka;
private Long bodovi;

    public EvaluacijaZnanjaDTO() {
        super();
    }

    public EvaluacijaZnanjaDTO(Long id, LocalDateTime vremePocetka, LocalDateTime vremeZavrsetka, Long bodovi) {
        this.id = id;
        this.vremePocetka = vremePocetka;
        this.vremeZavrsetka = vremeZavrsetka;
        this.bodovi = bodovi;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getVremepocetka() {
        return vremePocetka;
    }

    public void setVremepocetka(LocalDateTime vremePocetka) {
        this.vremePocetka = vremePocetka;
    }

    public LocalDateTime getVremezavrsetka() {
        return vremeZavrsetka;
    }

    public void setVremezavrsetka(LocalDateTime vremeZavrsetka) {
        this.vremeZavrsetka = vremeZavrsetka;
    }

    public Long getBodovi() {
        return bodovi;
    }

    public void setBodovi(Long bodovi) {
        this.bodovi = bodovi;
    }

}
