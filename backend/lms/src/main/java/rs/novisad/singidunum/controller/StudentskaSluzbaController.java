package rs.novisad.singidunum.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rs.novisad.singidunum.dto.GodinaStudijaDTO;
import rs.novisad.singidunum.dto.KorisnikDTO;
import rs.novisad.singidunum.dto.StudentskaSluzbaDTO;
import rs.novisad.singidunum.model.Fakultet;
import rs.novisad.singidunum.model.GodinaStudija;
import rs.novisad.singidunum.model.Korisnik;
import rs.novisad.singidunum.model.PravoPristupa;
import rs.novisad.singidunum.model.Predmet;
import rs.novisad.singidunum.model.Student;
import rs.novisad.singidunum.model.StudentNaGodini;
import rs.novisad.singidunum.model.StudentskaSluzba;
import rs.novisad.singidunum.model.StudijskiProgram;
import rs.novisad.singidunum.service.FakultetService;
import rs.novisad.singidunum.service.GodinaStudijaService;
import rs.novisad.singidunum.service.KorisnikService;
import rs.novisad.singidunum.service.PravoPristupaService;
import rs.novisad.singidunum.service.PredmetService;
import rs.novisad.singidunum.service.StudentService;
import rs.novisad.singidunum.service.StudentskaSluzbaService;
import rs.novisad.singidunum.service.StudijskiProgramService;

@RestController
@RequestMapping("/api/studentska-sluzba")
public class StudentskaSluzbaController {

    @Autowired
    private StudentskaSluzbaService studentskaSluzbaService;

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private PravoPristupaService pravoPristupaService;

    @Autowired
    private GodinaStudijaService godinaStudijaService;

    @Autowired
    private StudijskiProgramService studijskiProgramService;

    @Autowired
    private FakultetService fakultetService;

    @Autowired
    private PredmetService predmetService;

    @GetMapping
    public ResponseEntity<List<StudentskaSluzbaDTO>> dobaviSveFakultete() {
        Iterable<StudentskaSluzba> admini = studentskaSluzbaService.findAll();
        ArrayList<StudentskaSluzbaDTO> adminiDTO = new ArrayList<>();
        for (StudentskaSluzba admin : admini) {
            adminiDTO.add(new StudentskaSluzbaDTO(admin.getId(), admin.getUsername(), admin.getEmail()));
        }
        return ResponseEntity.ok(adminiDTO);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StudentskaSluzbaDTO> dobaviPoID(@PathVariable("id") Long id) {
        Optional<StudentskaSluzba> admin = studentskaSluzbaService.findOne(id);
        return admin.map(value -> ResponseEntity.ok(new StudentskaSluzbaDTO(value.getId(), value.getUsername(), value.getEmail())))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    // Korisnik studentske sluzbe moze da vidi sve registrovane korisnike i njihovo pravo pristua
    @GetMapping("/korisnici")
    public ResponseEntity<List<KorisnikDTO>> dobaviKorisnike() {
        Iterable<Korisnik> korisnici = korisnikService.findAll();
        ArrayList<KorisnikDTO> korisniciDTO = new ArrayList<>();
        for (Korisnik korisnik : korisnici) {
            korisniciDTO.add(new KorisnikDTO(korisnik.getUsername(), korisnik.getEmail(), korisnik.getPravoPristupa() != null ? korisnik.getPravoPristupa().getNaziv() : "NO_ROLE"));
        }
        return ResponseEntity.ok(korisniciDTO);
    }

     // Korisnik studentske sluzbe moze da dodeli ili promeni pravo pristupa registrovanih korisnika

    @PutMapping("/korisnici/{korisnickoIme}")
    public ResponseEntity<String> updatePravoPristupa(@PathVariable String korisnickoIme, @RequestParam String pravoPristupaNaziv) {
        Optional<Korisnik> korisnikOptional = korisnikService.findByUsername(korisnickoIme);
        if (korisnikOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Korisnik korisnik = korisnikOptional.get();
        PravoPristupa pravoPristupa = pravoPristupaService.findByNaziv(pravoPristupaNaziv).get();

        korisnik.setPravoPristupa(pravoPristupa);
        korisnikService.save(korisnik);
        return ResponseEntity.ok("Pravo pristupa uspesno izmenjeno.");
    }


    // Upisivanje studenta na skolsku godinu
    @PutMapping("/studenti/upisi/{korisnickoIme}")
    public ResponseEntity<String> upisiStudenta(@PathVariable String korisnickoIme, @RequestParam Long godina, @RequestParam String studijskiProgramNaziv) {
        try {
            Optional<Student> studentOptional = studentService.findOne(korisnickoIme);
            if (studentOptional.isEmpty()) {
                return ResponseEntity.notFound().build();
            }

            Student student = studentOptional.get();

            StudentNaGodini studentNaGodini = student.getStudentNaGodini();
            if (studentNaGodini == null) {
                studentNaGodini = new StudentNaGodini();
                student.setStudentNaGodini(studentNaGodini);
            }

            Optional<GodinaStudija> godinaStudijaOptional = godinaStudijaService.findByStudijskiProgramIdAndGodina(studijskiProgramService.findByNaziv(studijskiProgramNaziv).get().getId(), godina);
            if (godinaStudijaOptional.isEmpty()) {
                return ResponseEntity.notFound().build();
            }

            GodinaStudija godinaStudija = godinaStudijaOptional.get();
            studentNaGodini.setGodinaStudija(godinaStudija);

            godinaStudija.setStudijskiProgram(studijskiProgramService.findByNaziv(studijskiProgramNaziv).get());

            studentService.save(student);
            godinaStudijaService.save(godinaStudija);

            return ResponseEntity.ok().body("{\"message\": \"Student successfully enrolled.\"}");

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An error occurred: " + e.getMessage());
        }
    }

    // Dobavljanje studentskih godina za odabrani smer

    @GetMapping("/godina-studija/{naziv}")
    public ResponseEntity<List<GodinaStudijaDTO>> godinaStudija(@PathVariable String naziv) {
        try {
            Optional<StudijskiProgram> studijskiProgramOptional = studijskiProgramService.findByNaziv(naziv);

            if (studijskiProgramOptional.isEmpty()) {
                return ResponseEntity.notFound().build();
            }

            StudijskiProgram studijskiProgram = studijskiProgramOptional.get();
            Long studijskiProgramId = studijskiProgram.getId();

            Iterable<GodinaStudija> godineStudija = godinaStudijaService.findByStudijskiProgramId(studijskiProgramId);
            ArrayList<GodinaStudijaDTO> godineStudijaDTO = new ArrayList<>();
            for (GodinaStudija godinaStudija : godineStudija) {
                godineStudijaDTO.add(new GodinaStudijaDTO(godinaStudija.getGodina()));
            }

            return ResponseEntity.ok(godineStudijaDTO);
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }


// ADMINISTRACIJA STUDIJSKIH PROGRAMA

// Dodavanje studijskog programa
    @PostMapping("studijski-program")
    public ResponseEntity<Void> dodajStudijskiProgram(@RequestParam String naziv, @RequestParam String nazivFakulteta, @RequestParam Integer godine) {

        try {
            Fakultet fakultet = fakultetService.findByNaziv(nazivFakulteta).orElseThrow();

            StudijskiProgram studijskiProgram = new StudijskiProgram(naziv, fakultet);
            studijskiProgramService.save(studijskiProgram);

            Long godina = (long) 1;
            while (godina <= godine) {
                GodinaStudija godinaStudija = new GodinaStudija(godina, studijskiProgram);
                godinaStudijaService.save(godinaStudija);
                godina++;
            }

            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    // Brisanje studijskog programa
    @DeleteMapping("studijski-program/{naziv}")
    public ResponseEntity<Void> obrisiStudijskiProgram(@PathVariable String naziv) {
        try {
            Optional<StudijskiProgram> studijskiProgramOptional = studijskiProgramService.findByNaziv(naziv);
            if (studijskiProgramOptional.isEmpty()) {
                return ResponseEntity.notFound().build();
            }
    
            StudijskiProgram studijskiProgram = studijskiProgramOptional.get();
            Long id = studijskiProgram.getId();
            Iterable<GodinaStudija> godineStudija = godinaStudijaService.findByStudijskiProgramId(id);
    
            for (GodinaStudija godinaStudija : godineStudija) {
                Long godinaStudijaId = godinaStudija.getId();
                godinaStudijaService.delete(godinaStudijaId);
            }
    
            studijskiProgramService.delete(id);
    
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            // Log the exception or return a specific error response
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    

    // Dodavanje predmeta na studijske programe

    @PutMapping("/godina-studija/{id}/dodaj-predmet")
    public ResponseEntity<Void> dodajPredmetGodiniStudija(@PathVariable Long id, @RequestParam Long predmetId) {
        try {
            Optional<GodinaStudija> godinaStudijaOptional = godinaStudijaService.findOne(id);

            if (godinaStudijaOptional.isEmpty()) {
                return ResponseEntity.notFound().build();
            }

            Optional<Predmet> predmetOptional = predmetService.findOne(predmetId);

            if (predmetOptional.isEmpty()) {
                return ResponseEntity.notFound().build();
            }

            GodinaStudija godinaStudija = godinaStudijaOptional.get();
            Predmet predmet = predmetOptional.get();

            godinaStudija.getPredmeti().add(predmet);
            godinaStudijaService.save(godinaStudija);

            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    // Dodavanje novog predmeta na studijski program
    @PostMapping("/godina-studija/{id}/predmet")
    public ResponseEntity<Void> dodajNoviPredmetGodiniStudija(@PathVariable Long id, @RequestBody Predmet predmet) {
        try {
            Optional<GodinaStudija> godinaStudijaOptional = godinaStudijaService.findOne(id);

            if (godinaStudijaOptional.isEmpty()) {
                return ResponseEntity.notFound().build();
            }

            GodinaStudija godinaStudija = godinaStudijaOptional.get();

            predmet.setGodinaStudija(godinaStudija);
            predmetService.save(predmet);

            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody StudentskaSluzba admin) {
        studentskaSluzbaService.save(admin);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        studentskaSluzbaService.delete(id);
        return ResponseEntity.noContent().build();
    }
}

