package rs.novisad.singidunum.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.novisad.singidunum.model.NastavnikNaRealizaciji;

@Repository
public interface NastavnikNaRealizacijiRepository extends CrudRepository<NastavnikNaRealizaciji, Long>{

    Iterable<NastavnikNaRealizaciji> findByNastavnikId(Long nastavnikId);

}
