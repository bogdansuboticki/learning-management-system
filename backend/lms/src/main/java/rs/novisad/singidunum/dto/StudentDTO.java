package rs.novisad.singidunum.dto;

import java.time.LocalDate;


public class StudentDTO extends KorisnikDTO {
    private String ime;
    private String jmbg;
    private String brojIndeksa;
    private LocalDate datumUpisa;
    private String korisnickoIme;

    public StudentDTO() {
        super();
    }

    public StudentDTO(String ime, String brojIndeksa, LocalDate datumUpisa, String korisnickoIme)
    {
        this.ime = ime;
        this.brojIndeksa = brojIndeksa;
        this.datumUpisa = datumUpisa;
        this.korisnickoIme = korisnickoIme;
    }

    public StudentDTO(String korisnickoIme, String email, String ime, String jmbg) {
        super(korisnickoIme, email);
        this.ime = ime;
        this.jmbg = jmbg;
    }

    
    

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getJmbg() {
        return jmbg;
    }

    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }

    public String getBrojIndeksa() {
        return brojIndeksa;
    }

    public void setBrojIndeksa(String brojIndeksa) {
        this.brojIndeksa = brojIndeksa;
    }

    public LocalDate getDatumUpisa() {
        return datumUpisa;
    }

    public void setDatumUpisa(LocalDate datumUpisa) {
        this.datumUpisa = datumUpisa;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

}
