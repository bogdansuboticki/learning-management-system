package rs.novisad.singidunum.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.novisad.singidunum.model.Student;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {
    Optional<Student> findByUsername(String korisnickoIme);

}

