package rs.novisad.singidunum.dto;

public class FakultetDTO {

    private Long id;
    private String naziv;
    private Long univerzitetId;

    public FakultetDTO() {
        // Default constructor
    }

    public FakultetDTO(String naziv) {
        this.naziv = naziv;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Long getUniverzitetId() {
        return univerzitetId;
    }

    public void setUniverzitetId(Long univerzitetId) {
        this.univerzitetId = univerzitetId;
    }

    @Override
    public String toString() {
        return "FakultetDTO [id=" + id + ", naziv=" + naziv + ", univerzitetId=" + univerzitetId + "]";
    }

}
