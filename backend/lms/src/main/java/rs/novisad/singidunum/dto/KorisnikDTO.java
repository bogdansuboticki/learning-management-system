package rs.novisad.singidunum.dto;

public class KorisnikDTO {
    private String korisnickoIme;
    private String email;
    private String pravoPristupa;
    



    public KorisnikDTO() {
        super();
    }

    public KorisnikDTO(String korisnickoIme, String email) {
        this.korisnickoIme = korisnickoIme;
        this.email = email;

    }


    public KorisnikDTO(String korisnickoIme, String email, String pravoPristupa) {
        this.korisnickoIme = korisnickoIme;
        this.email = email;
        this.pravoPristupa = pravoPristupa;

    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPravoPristupa() {
        return pravoPristupa;
    }

    public void setPravoPristupa(String pravoPristupa) {
        this.pravoPristupa = pravoPristupa;
    }


    
    
}
