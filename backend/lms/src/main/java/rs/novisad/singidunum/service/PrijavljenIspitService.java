package rs.novisad.singidunum.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import rs.novisad.singidunum.dto.PrijavljenIspitDTO;
import rs.novisad.singidunum.model.EvaluacijaZnanja;
import rs.novisad.singidunum.model.Predmet;
import rs.novisad.singidunum.model.PrijavljenIspit;
import rs.novisad.singidunum.model.RealizacijaPredmeta;
import rs.novisad.singidunum.model.Student;
import rs.novisad.singidunum.model.TipEvaluacije;
import rs.novisad.singidunum.repository.EvaluacijaZnanjaRepository;
import rs.novisad.singidunum.repository.PredmetRepository;
import rs.novisad.singidunum.repository.PrijavljenIspitRepository;
import rs.novisad.singidunum.repository.StudentRepository;

@Service
public class PrijavljenIspitService {
    @Autowired
    private EvaluacijaZnanjaRepository evaluacijaZnanjaRepository;

    @Autowired
    private PrijavljenIspitRepository prijavljenIspitRepository;

    @Autowired
    private PredmetRepository predmetRepository;

    @Autowired
    private StudentRepository studentRepository;

    public Iterable<PrijavljenIspit> findAll() {
        return prijavljenIspitRepository.findAll();
    }

    public Optional<PrijavljenIspit> findOne(Long id) {
        return prijavljenIspitRepository.findById(id);
    }

    public void save(PrijavljenIspit prijavljenIspit) {
        prijavljenIspitRepository.save(prijavljenIspit);
    }

    public void delete(Long id) {
        prijavljenIspitRepository.deleteById(id);
    }

    public void delete(PrijavljenIspit prijavljenIspit) {
        prijavljenIspitRepository.delete(prijavljenIspit);
    }


    @Transactional
    public void prijaviIspit(Long evaluacijaZnanjaId, Long predmetId, String username) {
        try {
            Optional<EvaluacijaZnanja> evaluacijaZnanja = evaluacijaZnanjaRepository.findById(evaluacijaZnanjaId);
            if (evaluacijaZnanja.isEmpty()) {
                throw new EntityNotFoundException("Nije pronadjen ispit sa ovim id:: " + evaluacijaZnanjaId);
            }
            
            TipEvaluacije tip = evaluacijaZnanja.get().getTipEvaluacije();
            if (!tip.getNaziv().equals("Ispit")) {
                throw new IllegalStateException("Mozete prijaviti samo ispit");
            }
    
            Optional<Predmet> predmet = predmetRepository.findById(predmetId);
            if (predmet.isEmpty()) {
                throw new EntityNotFoundException("Ne postoji predmet sa ovim id: " + predmetId);
            }
    
            Optional<Student> student = studentRepository.findByUsername(username);
            if (student.isEmpty()) {
                throw new EntityNotFoundException("Ne postoji student sa ovim korisnickim imenom " + username);
            }
    
            PrijavljenIspit prijavljenIspit = new PrijavljenIspit();
            RealizacijaPredmeta realizacijaPredmeta = evaluacijaZnanja.get().getRealizacijaPredmeta();
            realizacijaPredmeta.setPredmet(predmet.get());
            prijavljenIspit.setPrijavljen(true);
            prijavljenIspit.setEvaluacijaZnanja(evaluacijaZnanja.get());
            prijavljenIspit.setStudent(student.get());
    
            prijavljenIspitRepository.save(prijavljenIspit);
        } catch (EntityNotFoundException | IllegalStateException e) {
            throw e; 
        } catch (Exception e) {
            throw new RuntimeException("Greska prilikom prijave ispita.", e);
        }
    }
    
    


    public List<PrijavljenIspitDTO> getPrijavljeniIspiti(String studentUsername) {
        Optional<Student> studentOptional = studentRepository.findByUsername(studentUsername);
        if (studentOptional.isEmpty()) {
            throw new EntityNotFoundException("Nije pronadjen student sa ovim korisnickim imenom:  " + studentUsername);
        }
    
        Student student = studentOptional.get();
        List<PrijavljenIspit> prijavljenIspitList = prijavljenIspitRepository.findAllByStudentId(student.getId());
        if (prijavljenIspitList.isEmpty()) {
            throw new EntityNotFoundException("Nema prijavljenih ispita za studenta sa ovim korisnickim imenom: " + studentUsername);
        }


        List<PrijavljenIspitDTO> prijavljeniIspitiDTO = new ArrayList<>();
        for (PrijavljenIspit prijavljenIspit : prijavljenIspitList) {
            System.out.println("Prijavljen ispit: "+  prijavljenIspit.getId());
            EvaluacijaZnanja evaluacijaZnanja = prijavljenIspit.getEvaluacijaZnanja();


            String nazivPredmeta = evaluacijaZnanja.getRealizacijaPredmeta().getPredmet().getNaziv();
            Long espb = evaluacijaZnanja.getRealizacijaPredmeta().getPredmet().getEspb();
            String nastavnik = evaluacijaZnanja.getRealizacijaPredmeta().getNastavnikNaRealizaciji().getNastavnik().getIme();
            prijavljeniIspitiDTO.add(new PrijavljenIspitDTO(nazivPredmeta, nastavnik, espb, prijavljenIspit.isPrijavljen()));

        }
    
        return prijavljeniIspitiDTO;
    }
    

}
