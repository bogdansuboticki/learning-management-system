package rs.novisad.singidunum.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.model.Nastavnik;
import rs.novisad.singidunum.repository.NastavnikRepository;

@Service
public class NastavnikService {
    @Autowired
    private NastavnikRepository nastavnikRepository;

    public Iterable<Nastavnik> findAll() {
        return nastavnikRepository.findAll();
    }

    public Optional<Nastavnik> findOne(Long id) {
        return nastavnikRepository.findById(id);
    }

    public Optional<Nastavnik> findOne(String korisnickoIme) {
        return nastavnikRepository.findByUsername(korisnickoIme);
    }

    public void save(Nastavnik nastavnik) {
        nastavnikRepository.save(nastavnik);
    }

    public void delete(Long id) {
        nastavnikRepository.deleteById(id);
    }

    public void delete(Nastavnik nastavnik) {
        nastavnikRepository.delete(nastavnik);
    }
}
