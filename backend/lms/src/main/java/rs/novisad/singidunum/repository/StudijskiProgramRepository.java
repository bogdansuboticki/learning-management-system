package rs.novisad.singidunum.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import rs.novisad.singidunum.model.StudijskiProgram;
import org.springframework.stereotype.Repository;
@Repository
public interface StudijskiProgramRepository extends CrudRepository<StudijskiProgram, Long>{
    Optional<StudijskiProgram> findByNaziv(String naziv);
}
