package rs.novisad.singidunum.service;

import java.util.Optional;

import javax.swing.text.html.Option;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.model.StudijskiProgram;
import rs.novisad.singidunum.repository.StudijskiProgramRepository;

@Service
public class StudijskiProgramService {
    @Autowired
    private StudijskiProgramRepository studijskiProgramRepository;

    public Iterable<StudijskiProgram> findAll() {
        return studijskiProgramRepository.findAll();
    }

    public Optional<StudijskiProgram> findByNaziv(String naziv){
        return studijskiProgramRepository.findByNaziv(naziv);
    }

    public Optional<StudijskiProgram> findOne(Long id) {
        return studijskiProgramRepository.findById(id);
    }

    public void save(StudijskiProgram studijskiProgram) {
        studijskiProgramRepository.save(studijskiProgram);
    }

    public void delete(Long id) {
        studijskiProgramRepository.deleteById(id);
    }

    public void delete(StudijskiProgram studijskiProgram) {
        studijskiProgramRepository.delete(studijskiProgram);
    }
}
