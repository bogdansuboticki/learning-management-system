package rs.novisad.singidunum.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.model.Korisnik;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    KorisnikService korisnikService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Korisnik> korisnik = korisnikService.findByUsername(username);
        if (korisnik.isPresent()) {
            ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            grantedAuthorities.add(new SimpleGrantedAuthority(korisnik.get().getPravoPristupa().getNaziv()));
            return new User(korisnik.get().getUsername(), korisnik.get().getPassword(), grantedAuthorities);
        }
        return null;
    }

}
