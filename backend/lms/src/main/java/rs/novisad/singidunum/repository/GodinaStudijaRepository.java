package rs.novisad.singidunum.repository;



import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.novisad.singidunum.model.GodinaStudija;

@Repository
public interface GodinaStudijaRepository extends CrudRepository<GodinaStudija, Long> {
    Iterable<GodinaStudija> findByStudijskiProgramId(Long studijskiProgramId);
    Optional<GodinaStudija> findByStudijskiProgramIdAndGodina(Long studijskiProgramId, Long godina);
}
