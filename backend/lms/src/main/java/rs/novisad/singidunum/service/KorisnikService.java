package rs.novisad.singidunum.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.model.Korisnik;
import rs.novisad.singidunum.repository.KorisnikRepository;

@Service
public class KorisnikService {
    @Autowired
    private KorisnikRepository korisnikRepository;

    public Iterable<Korisnik> findAll() {
        return korisnikRepository.findAll();
    }

    public Optional<Korisnik> findOne(Long id) {
        return korisnikRepository.findById(id);
    }

    public Optional<Korisnik> findByUsername(String username)
    {
        return korisnikRepository.findByUsername(username);
    }

    public void save(Korisnik korisnik) {
        korisnikRepository.save(korisnik);
    }

    public void delete(Long id) {
        korisnikRepository.deleteById(id);
    }

    public void delete(Korisnik korisnik) {
        korisnikRepository.delete(korisnik);
    }

    public Korisnik createKorisnik(String korisnickoIme, String email, String password) {
        Korisnik korisnik = new Korisnik(korisnickoIme, email, password);
        return korisnikRepository.save(korisnik);
    }
}

