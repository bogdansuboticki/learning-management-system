package rs.novisad.singidunum.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;

@Entity
public class NastavnikNaRealizaciji {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    Long brojCasova;


    @ManyToOne
    private Nastavnik nastavnik;

    @OneToOne(mappedBy = "nastavnikNaRealizaciji")
    private TipNastave tipNastave;

    @OneToOne(mappedBy = "nastavnikNaRealizaciji")
    private RealizacijaPredmeta realizacijaPredmeta;

    public NastavnikNaRealizaciji() {
    }

    public NastavnikNaRealizaciji(Long id, Long brojCasova, Nastavnik nastavnik, TipNastave tipNastave,
            RealizacijaPredmeta realizacijaPredmeta) {
        this.id = id;
        this.brojCasova = brojCasova;
        this.nastavnik = nastavnik;
        this.tipNastave = tipNastave;
        this.realizacijaPredmeta = realizacijaPredmeta;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBrojCasova() {
        return brojCasova;
    }

    public void setBrojCasova(Long brojCasova) {
        this.brojCasova = brojCasova;
    }

    public Nastavnik getNastavnik() {
        return nastavnik;
    }

    public void setNastavnik(Nastavnik nastavnik) {
        this.nastavnik = nastavnik;
    }

    public TipNastave getTipNastave() {
        return tipNastave;
    }

    public void setTipNastave(TipNastave tipNastave) {
        this.tipNastave = tipNastave;
    }

    public RealizacijaPredmeta getRealizacijaPredmeta() {
        return realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
        this.realizacijaPredmeta = realizacijaPredmeta;
    }


    

}
