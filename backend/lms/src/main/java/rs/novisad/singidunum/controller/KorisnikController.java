package rs.novisad.singidunum.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rs.novisad.singidunum.dto.KorisnikDTO;
import rs.novisad.singidunum.model.Korisnik;
import rs.novisad.singidunum.service.KorisnikService;

@RestController
// @Secured({"ROLE_ADMIN"})
@RequestMapping("/api/korisnici")
public class KorisnikController {
    @Autowired
    private KorisnikService korisnikService;

    @GetMapping
    public ResponseEntity<List<KorisnikDTO>> dobaviSveKorisnike(){
        Iterable<Korisnik> korisnici = korisnikService.findAll();
        ArrayList<KorisnikDTO> korisniciDTO = new ArrayList<>();
        for(Korisnik korisnik : korisnici){
            korisniciDTO.add(new KorisnikDTO(korisnik.getUsername(), korisnik.getEmail()));
        }
        return new ResponseEntity<>(korisniciDTO, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<KorisnikDTO> dobaviPoID(@PathVariable("id") Long id){
        Optional<Korisnik> korisnik = korisnikService.findOne(id);
        if(korisnik.isPresent()){
            KorisnikDTO korisnikDTO = new KorisnikDTO(korisnik.get().getUsername(), korisnik.get().getEmail());
            return new ResponseEntity<>(korisnikDTO, HttpStatus.OK);
        }
        return new ResponseEntity<KorisnikDTO>(HttpStatus.NOT_FOUND);
    }


    

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody Korisnik korisnik) {
        korisnikService.save(korisnik);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        korisnikService.delete(id);
        return ResponseEntity.noContent().build();
    }
}

