package rs.novisad.singidunum.dto;

import java.util.Date;

public class StudentNaGodiniDTO {

    private Long id;
    private Date datumUpisa;
    private String brojIndeksa;
    private Long godinaStudijaId;
    private Long studentId;

    public StudentNaGodiniDTO() {
        // Default constructor
    }

    public StudentNaGodiniDTO(Long id, Date datumUpisa, String brojIndeksa, Long godinaStudijaId, Long studentId) {
        this.id = id;
        this.datumUpisa = datumUpisa;
        this.brojIndeksa = brojIndeksa;
        this.godinaStudijaId = godinaStudijaId;
        this.studentId = studentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDatumUpisa() {
        return datumUpisa;
    }

    public void setDatumUpisa(Date datumUpisa) {
        this.datumUpisa = datumUpisa;
    }

    public String getBrojIndeksa() {
        return brojIndeksa;
    }

    public void setBrojIndeksa(String brojIndeksa) {
        this.brojIndeksa = brojIndeksa;
    }

    public Long getGodinaStudijaId() {
        return godinaStudijaId;
    }

    public void setGodinaStudijaId(Long godinaStudijaId) {
        this.godinaStudijaId = godinaStudijaId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    @Override
    public String toString() {
        return "StudentNaGodiniDTO [id=" + id + ", datumUpisa=" + datumUpisa + ", brojIndeksa=" + brojIndeksa
                + ", godinaStudijaId=" + godinaStudijaId + ", studentId=" + studentId + "]";
    }

}
