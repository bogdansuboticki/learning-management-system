package rs.novisad.singidunum.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.novisad.singidunum.model.Nastavnik;

@Repository
public interface NastavnikRepository extends CrudRepository<Nastavnik, Long> {
    Optional<Nastavnik> findByUsername(String korisnickoIme);
}
