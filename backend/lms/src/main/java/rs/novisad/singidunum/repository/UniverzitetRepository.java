package rs.novisad.singidunum.repository;

import org.springframework.data.repository.CrudRepository;
import rs.novisad.singidunum.model.Univerzitet;
import org.springframework.stereotype.Repository;
@Repository
public interface UniverzitetRepository extends CrudRepository<Univerzitet, Long>{

}
