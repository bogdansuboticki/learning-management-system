package rs.novisad.singidunum.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.novisad.singidunum.model.PravoPristupa;

@Repository
public interface PravoPristupaRepository extends CrudRepository<PravoPristupa, Long>{
    Optional<PravoPristupa> findByIdAndKorisniciId(Long pravoPristupaId, Long korisnikId);
    Optional<PravoPristupa> findByNaziv(String naziv);
}
