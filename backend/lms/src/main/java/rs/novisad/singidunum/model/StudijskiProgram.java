package rs.novisad.singidunum.model;

import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class StudijskiProgram {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String naziv;

    @ManyToOne()
    private Fakultet fakultet;

    @OneToMany(mappedBy = "studijskiProgram")
    private List<GodinaStudija> godineStudija;

    public StudijskiProgram() {
        // Default constructor
    }

    public StudijskiProgram(String naziv, Fakultet fakultet) {
        this.naziv = naziv;
        this.fakultet = fakultet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Fakultet getFakultet() {
        return fakultet;
    }

    public void setFakultet(Fakultet fakultet) {
        this.fakultet = fakultet;
    }

    public List<GodinaStudija> getGodineStudija() {
        return godineStudija;
    }

    public void setGodineStudija(List<GodinaStudija> godineStudija) {
        this.godineStudija = godineStudija;
    }

}
