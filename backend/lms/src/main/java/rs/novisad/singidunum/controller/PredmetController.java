package rs.novisad.singidunum.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.novisad.singidunum.dto.PredmetDTO;
import rs.novisad.singidunum.model.Predmet;
import rs.novisad.singidunum.service.PredmetService;

@Controller
@RequestMapping(path = "/api")
public class PredmetController {
    @Autowired
    private PredmetService predmetService;

    @RequestMapping(path = "/predmet", method = RequestMethod.GET)
    public ResponseEntity<List<PredmetDTO>> dobaviSvePredmete(){
        Iterable<Predmet> predmeti = predmetService.findAll();
        ArrayList<PredmetDTO> predmetiDTO = new ArrayList<>();
        for(Predmet predmet : predmeti){
            predmetiDTO.add(new PredmetDTO(predmet.getId(), predmet.getNaziv(), predmet.getEspb(), predmet.getObavezan(), predmet.getBrojPredavanja(), predmet.getBrojVezbi(), predmet.getDrugiObliciNastave(), predmet.getIstrazivackiRad(), predmet.getOstaliCasovi()));
        }
        return new ResponseEntity<>(predmetiDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "/predmet/{id}", method = RequestMethod.GET)
    public ResponseEntity<PredmetDTO> dobaviPoID(@PathVariable("id") Long id){
        Optional<Predmet> predmet = predmetService.findOne(id);
        if(predmet.isPresent()){
            PredmetDTO predmetDTO = new PredmetDTO(predmet.get().getId(), predmet.get().getNaziv(), predmet.get().getEspb(), predmet.get().getObavezan(), predmet.get().getBrojPredavanja(), predmet.get().getBrojVezbi(), predmet.get().getDrugiObliciNastave(), predmet.get().getIstrazivackiRad(), predmet.get().getOstaliCasovi());
            return new ResponseEntity<>(predmetDTO, HttpStatus.OK);
        }
        return new ResponseEntity<PredmetDTO>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/predmet", method = RequestMethod.POST)
    public ResponseEntity<Void> dodajPredmet(@RequestBody PredmetDTO predmet){
        predmetService.save(new Predmet(predmet.getNaziv(), predmet.getEspb(), predmet.getObavezan(), predmet.getBrojPredavanja(), predmet.getBrojVezbi(), predmet.getDrugiObliciNastave(), predmet.getIstrazivackiRad(), predmet.getOstaliCasovi()));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(path = "/predmet/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<PredmetDTO> ukloniPoId(@PathVariable("id") Long id){
        if(predmetService.findOne(id).isPresent()){
            predmetService.delete(id);
            return new ResponseEntity<PredmetDTO>(HttpStatus.GONE);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}

