package rs.novisad.singidunum.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import rs.novisad.singidunum.model.Predmet;
import org.springframework.stereotype.Repository;
@Repository
public interface PredmetRepository extends CrudRepository<Predmet, Long>{
    Optional<Predmet> findByNaziv(String naziv);
}
