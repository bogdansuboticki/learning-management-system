package rs.novisad.singidunum.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.model.NastavnikNaRealizaciji;
import rs.novisad.singidunum.repository.NastavnikNaRealizacijiRepository;

@Service
public class NastavnikNaRealizacijiService {

    @Autowired
    private NastavnikNaRealizacijiRepository nastavnikNaRealizacijiRepository;

    public Iterable<NastavnikNaRealizaciji> findAll() {
        return nastavnikNaRealizacijiRepository.findAll();
    }

    public Iterable<NastavnikNaRealizaciji> findAll(Long nastavnikId) {
        return nastavnikNaRealizacijiRepository.findByNastavnikId(nastavnikId);
    }

    public Optional<NastavnikNaRealizaciji> findOne(Long id) {
        return nastavnikNaRealizacijiRepository.findById(id);
    }

    public void save(NastavnikNaRealizaciji nastavnikNaRealizaciji) {
        nastavnikNaRealizacijiRepository.save(nastavnikNaRealizaciji);
    }

    public void delete(Long id) {
        nastavnikNaRealizacijiRepository.deleteById(id);
    }

    public void delete(NastavnikNaRealizaciji nastavnikNaRealizaciji) {
        nastavnikNaRealizacijiRepository.delete(nastavnikNaRealizaciji);
    }

}
