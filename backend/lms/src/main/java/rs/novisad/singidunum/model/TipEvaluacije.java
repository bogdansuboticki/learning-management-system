package rs.novisad.singidunum.model;

import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;

@Entity
public class TipEvaluacije {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String naziv;

    @OneToMany(mappedBy = "tipEvaluacije")
    private List<EvaluacijaZnanja> evaluacijaZnanja;

    

    public List<EvaluacijaZnanja> getEvaluacijaZnanja() {
        return evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja(List<EvaluacijaZnanja> evaluacijaZnanja) {
        this.evaluacijaZnanja = evaluacijaZnanja;
    }

    public TipEvaluacije() {
    }

    public TipEvaluacije(Long id, String naziv) {
        this.id = id;
        this.naziv = naziv;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }


    

}
