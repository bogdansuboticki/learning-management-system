package rs.novisad.singidunum.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;

@Entity
public class Predmet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String naziv;
    private Long espb;
    private Boolean obavezan;
    private Long brojPredavanja;
    private Long brojVezbi;
    private Long drugiObliciNastave;
    private Long istrazivackiRad;
    private Long ostaliCasovi;

    @ManyToOne
    private GodinaStudija godinaStudija;

    @OneToOne(mappedBy = "predmet")
    private RealizacijaPredmeta realizacijaPredmeta;


    public Predmet() {
        // Default constructor
    }

    public Predmet(String naziv, Long espb, Boolean obavezan, Long brojPredavanja, Long brojVezbi,
            Long drugiObliciNastave, Long istrazivackiRad, Long ostaliCasovi) {
        this.naziv = naziv;
        this.espb = espb;
        this.obavezan = obavezan;
        this.brojPredavanja = brojPredavanja;
        this.brojVezbi = brojVezbi;
        this.drugiObliciNastave = drugiObliciNastave;
        this.istrazivackiRad = istrazivackiRad;
        this.ostaliCasovi = ostaliCasovi;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Long getEspb() {
        return espb;
    }

    public void setEspb(Long espb) {
        this.espb = espb;
    }

    public Boolean getObavezan() {
        return obavezan;
    }

    public void setObavezan(Boolean obavezan) {
        this.obavezan = obavezan;
    }

    public Long getBrojPredavanja() {
        return brojPredavanja;
    }

    public void setBrojPredavanja(Long brojPredavanja) {
        this.brojPredavanja = brojPredavanja;
    }

    public Long getBrojVezbi() {
        return brojVezbi;
    }

    public void setBrojVezbi(Long brojVezbi) {
        this.brojVezbi = brojVezbi;
    }

    public Long getDrugiObliciNastave() {
        return drugiObliciNastave;
    }

    public void setDrugiObliciNastave(Long drugiObliciNastave) {
        this.drugiObliciNastave = drugiObliciNastave;
    }

    public Long getIstrazivackiRad() {
        return istrazivackiRad;
    }

    public void setIstrazivackiRad(Long istrazivackiRad) {
        this.istrazivackiRad = istrazivackiRad;
    }

    public Long getOstaliCasovi() {
        return ostaliCasovi;
    }

    public void setOstaliCasovi(Long ostaliCasovi) {
        this.ostaliCasovi = ostaliCasovi;
    }

    public GodinaStudija getGodinaStudija() {
        return godinaStudija;
    }

    public void setGodinaStudija(GodinaStudija godinaStudija) {
        this.godinaStudija = godinaStudija;
    }

    public RealizacijaPredmeta getRealizacijaPredmeta() {
        return realizacijaPredmeta;
    }

    public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
        this.realizacijaPredmeta = realizacijaPredmeta;
    }

}
