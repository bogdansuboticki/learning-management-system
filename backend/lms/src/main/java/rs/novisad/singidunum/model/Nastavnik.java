package rs.novisad.singidunum.model;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;


@Entity
public class Nastavnik extends Korisnik{

private String ime;

@Column(unique = true)
private String jmbg;

private String biografija;

@OneToMany(mappedBy = "nastavnik")
private List<NastavnikNaRealizaciji> nastavnikNaRealizaciji;


    public Nastavnik() {
    }

    public Nastavnik(String ime, String jmbg, String biografija) {
        this.ime = ime;
        this.jmbg = jmbg;
        this.biografija = biografija;
    }

    public Nastavnik(String korisnickoIme, String email, String password, String ime, String jmbg, String biografija) {
        super(korisnickoIme, email, password);
        this.ime = ime;
        this.jmbg = jmbg;
        this.biografija = biografija;
    }



    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getJmbg() {
        return jmbg;
    }

    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }

    public String getBiografija() {
        return biografija;
    }

    public void setBiografija(String biografija) {
        this.biografija = biografija;
    }





}

