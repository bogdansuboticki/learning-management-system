package rs.novisad.singidunum.model;



import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;

@Entity
public class PrijavljenIspit {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    boolean prijavljen;

    @OneToOne(mappedBy = "prijavljenIspit")
    private EvaluacijaZnanja evaluacijaZnanja;  
    
    @ManyToOne
    private Student student;


    public PrijavljenIspit() {
    }
    
    public PrijavljenIspit(Long id, boolean prijavljen, EvaluacijaZnanja evaluacijaZnanja) {
        this.id = id;
        this.prijavljen = prijavljen;
        this.evaluacijaZnanja = evaluacijaZnanja;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isPrijavljen() {
        return prijavljen;
    }

    public void setPrijavljen(boolean prijavljen) {
        this.prijavljen = prijavljen;
    }

    

    public EvaluacijaZnanja getEvaluacijaZnanja() {
        return evaluacijaZnanja;
    }

    public void setEvaluacijaZnanja(EvaluacijaZnanja evaluacijaZnanja) {
        this.evaluacijaZnanja = evaluacijaZnanja;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
    

}
