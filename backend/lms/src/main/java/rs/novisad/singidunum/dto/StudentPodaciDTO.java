package rs.novisad.singidunum.dto;


import java.util.List;

public class StudentPodaciDTO {

    private String ime;
    private String jmbg;
    private String brojIndeksa;

    private List<PolozeniIspitiDTO> polozeni;

   


    public StudentPodaciDTO(String ime, String jmbg, String brojIndeksa,
            List<PolozeniIspitiDTO> polozeni) {
        this.ime = ime;
        this.jmbg = jmbg;
        this.brojIndeksa = brojIndeksa;
        this.polozeni = polozeni;
    }
    public List<PolozeniIspitiDTO> getPolozeni() {
        return polozeni;
    }
    public void setPolozeni(List<PolozeniIspitiDTO> polozeni) {
        this.polozeni = polozeni;
    }

    public String getIme() {
        return ime;
    }
    public void setIme(String ime) {
        this.ime = ime;
    }
    public String getJmbg() {
        return jmbg;
    }
    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }
    public String getBrojIndeksa() {
        return brojIndeksa;
    }
    public void setBrojIndeksa(String brojIndeksa) {
        this.brojIndeksa = brojIndeksa;
    }
  
    
    

}
