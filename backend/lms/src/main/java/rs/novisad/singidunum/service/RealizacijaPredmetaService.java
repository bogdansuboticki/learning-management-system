package rs.novisad.singidunum.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.novisad.singidunum.model.RealizacijaPredmeta;
import rs.novisad.singidunum.repository.RealizacijaPredmetaRepository;

@Service
public class RealizacijaPredmetaService {
    @Autowired
    private RealizacijaPredmetaRepository realizacijapredmetaRepository;

    public Iterable<RealizacijaPredmeta> findAll() {
        return realizacijapredmetaRepository.findAll();
    }

    public Optional<RealizacijaPredmeta> findOne(Long id) {
        return realizacijapredmetaRepository.findById(id);
    }

    public void save(RealizacijaPredmeta realizacijapredmeta) {
        realizacijapredmetaRepository.save(realizacijapredmeta);
    }

    public void delete(Long id) {
        realizacijapredmetaRepository.deleteById(id);
    }

    public void delete(RealizacijaPredmeta realizacijapredmeta) {
        realizacijapredmetaRepository.delete(realizacijapredmeta);
    }

}
