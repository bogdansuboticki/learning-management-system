package rs.novisad.singidunum.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.novisad.singidunum.model.PrijavljenIspit;

@Repository
public interface PrijavljenIspitRepository extends CrudRepository<PrijavljenIspit, Long> {
    List<PrijavljenIspit> findAllByStudentId(Long id);

}
