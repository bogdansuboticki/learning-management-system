package rs.novisad.singidunum.model;

import java.time.LocalDate;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;

@Entity
public class StudentNaGodini {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDate datumUpisa;
    private String brojIndeksa;

    @ManyToOne
    private GodinaStudija godinaStudija;

    @OneToOne
    private Student student;

    @OneToMany(mappedBy = "studentNaGodini")
    private List<Polaganje> polaganje;


    public StudentNaGodini() {
        // Default constructor
    }

    public StudentNaGodini(LocalDate datumUpisa, String brojIndeksa, GodinaStudija godinaStudija, Student student) {
        this.datumUpisa = datumUpisa;
        this.brojIndeksa = brojIndeksa;
        this.godinaStudija = godinaStudija;
        this.student = student;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDatumUpisa() {
        return datumUpisa;
    }

    public void setDatumUpisa(LocalDate datumUpisa) {
        this.datumUpisa = datumUpisa;
    }

    public String getBrojIndeksa() {
        return brojIndeksa;
    }

    public void setBrojIndeksa(String brojIndeksa) {
        this.brojIndeksa = brojIndeksa;
    }

    public GodinaStudija getGodinaStudija() {
        return godinaStudija;
    }

    public void setGodinaStudija(GodinaStudija godinaStudija) {
        this.godinaStudija = godinaStudija;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

}
