package rs.novisad.singidunum.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.novisad.singidunum.dto.FakultetDTO;
import rs.novisad.singidunum.model.Fakultet;
import rs.novisad.singidunum.service.FakultetService;

@Controller
@RequestMapping(path = "/api")
public class FakultetController {

    @Autowired
    private FakultetService fakultetService;


    @RequestMapping(path = "/fakultet", method = RequestMethod.GET)
    public ResponseEntity<List<FakultetDTO>> dobaviSveFakultete(){
        Iterable<Fakultet> fakulteti = fakultetService.findAll();
        ArrayList<FakultetDTO> fakultetiDTO = new ArrayList<>();
        for(Fakultet fakultet : fakulteti){
            fakultetiDTO.add(new FakultetDTO(fakultet.getNaziv()));
        }
        return new ResponseEntity<>(fakultetiDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "/fakultet/{id}", method = RequestMethod.GET)
    public ResponseEntity<FakultetDTO> dobaviPoID(@PathVariable("id") Long id){
        Optional<Fakultet> fakultet = fakultetService.findOne(id);
        if(fakultet.isPresent()){
            FakultetDTO fakultetDTO = new FakultetDTO(fakultet.get().getNaziv());
            return new ResponseEntity<>(fakultetDTO, HttpStatus.OK);
        }
        return new ResponseEntity<FakultetDTO>(HttpStatus.NOT_FOUND);
    }
}

