package rs.novisad.singidunum.dto;

public class StudijskiProgramDTO {

    private Long id;
    private String naziv;
    private String fakultet;
    private int brojGodina;

    public StudijskiProgramDTO() {
        // Default constructor
    }

    public StudijskiProgramDTO(String naziv, String fakultet, int brojGodina) {
        this.naziv = naziv;
        this.fakultet = fakultet;
        this.brojGodina = brojGodina;
    }
    public StudijskiProgramDTO(String naziv) {
        this.naziv = naziv;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getFakultet() {
        return fakultet;
    }

    public void setFakultet(String fakultet) {
        this.fakultet = fakultet;
    }

    public int getBrojGodina() {
        return brojGodina;
    }

    public void setBrojGodina(int brojGodina) {
        this.brojGodina = brojGodina;
    }



}
