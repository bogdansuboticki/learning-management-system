package rs.novisad.singidunum.model;

import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;

@Entity
public class TerminNastave {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    LocalDateTime vremePocetka;
    LocalDateTime vremeZavrsetka;

    @OneToOne
    private TipNastave tipNastave;

    @OneToOne
    private RealizacijaPredmeta realizacijaPredmeta;


}
