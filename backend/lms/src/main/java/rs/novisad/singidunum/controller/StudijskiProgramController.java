package rs.novisad.singidunum.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rs.novisad.singidunum.dto.StudijskiProgramDTO;
import rs.novisad.singidunum.model.Fakultet;
import rs.novisad.singidunum.model.GodinaStudija;
import rs.novisad.singidunum.model.StudijskiProgram;
import rs.novisad.singidunum.service.StudijskiProgramService;

@RestController
@RequestMapping("/api/studijski-program")
public class StudijskiProgramController {
    @Autowired
    private StudijskiProgramService studijskiProgramService;

    @GetMapping
    public ResponseEntity<List<StudijskiProgramDTO>> dobaviNaziveSvihStudijskihPrograma(){
        Iterable<StudijskiProgram> studijskiProgrami = studijskiProgramService.findAll();
        ArrayList<StudijskiProgramDTO> studijskiProgramiDTO = new ArrayList<>();
        for(StudijskiProgram studijskiProgram : studijskiProgrami){
            studijskiProgramiDTO.add(new StudijskiProgramDTO(studijskiProgram.getNaziv()));
        }
        return new ResponseEntity<>(studijskiProgramiDTO, HttpStatus.OK);
    }


    @GetMapping("/svi")
    public ResponseEntity<List<StudijskiProgramDTO>> dobaviSveStudijskePrograme(){
        Iterable<StudijskiProgram> studijskiProgrami = studijskiProgramService.findAll();
        ArrayList<StudijskiProgramDTO> studijskiProgramiDTO = new ArrayList<>();
       
        for(StudijskiProgram studijskiProgram : studijskiProgrami){
            int brojGodina = 0;
            Fakultet fakultet = studijskiProgram.getFakultet();
            Iterable<GodinaStudija> godineStudija = studijskiProgram.getGodineStudija();
            for(GodinaStudija godinaStudija : godineStudija)
            {
                brojGodina++;
            }
            studijskiProgramiDTO.add(new StudijskiProgramDTO(studijskiProgram.getNaziv(), fakultet.getNaziv(), brojGodina));
        }
        return new ResponseEntity<>(studijskiProgramiDTO, HttpStatus.OK);
    }
}
