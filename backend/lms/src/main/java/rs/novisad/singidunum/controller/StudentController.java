package rs.novisad.singidunum.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rs.novisad.singidunum.dto.GodinaStudijaDTO;
import rs.novisad.singidunum.dto.PolozeniIspitiDTO;
import rs.novisad.singidunum.dto.PredmetDTO;
import rs.novisad.singidunum.dto.StudentDTO;
import rs.novisad.singidunum.dto.StudijskiProgramDTO;
import rs.novisad.singidunum.model.GodinaStudija;
import rs.novisad.singidunum.model.Korisnik;
import rs.novisad.singidunum.model.Predmet;
import rs.novisad.singidunum.model.Student;
import rs.novisad.singidunum.model.StudentNaGodini;
import rs.novisad.singidunum.model.StudijskiProgram;
import rs.novisad.singidunum.security.SecurityConfiguration;
import rs.novisad.singidunum.service.KorisnikService;
import rs.novisad.singidunum.service.StudentService;


@RestController
@RequestMapping("/api/studenti")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @Autowired
    SecurityConfiguration securityConfiguration;

    @Autowired
    private KorisnikService korisnikService;

    @GetMapping
    public ResponseEntity<List<StudentDTO>> dobaviSveStudente(){
        Iterable<Student> studenti = studentService.findAll();
        ArrayList<StudentDTO> studentiDTO = new ArrayList<>();
        for(Student student : studenti){
            studentiDTO.add(new StudentDTO(student.getUsername(), student.getEmail(), student.getIme(), student.getJmbg()));
        }
        return new ResponseEntity<>(studentiDTO, HttpStatus.OK);
    }

    @GetMapping("/{korisnickoIme}")
    public ResponseEntity<StudentDTO> dobaviPoKorisnickomImenu(@PathVariable("korisnickoIme") String korisnickoIme){
        Optional<Student> student = studentService.findOne(korisnickoIme);
        if(student.isPresent()){
            StudentDTO studentDTO = new StudentDTO(student.get().getUsername(), student.get().getEmail() ,student.get().getIme(), student.get().getJmbg());
            return new ResponseEntity<>(studentDTO, HttpStatus.OK);
        }
        return new ResponseEntity<StudentDTO>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{korisnickoIme}")
    public ResponseEntity<StudentDTO> izmeniStudenta(@PathVariable("korisnickoIme") String korisnickoIme, @RequestBody StudentDTO student) {
        Optional<Student> st = studentService.findOne(korisnickoIme);
        if(st.isPresent() && st.get().getUsername().equals(student.getKorisnickoIme())) {
            Student zaIzmenu = st.get();
            // zaIzmenu.setPassword(securityConfiguration.getPasswordEncoder().encode(student.getPassword()));
		    zaIzmenu.setEmail(student.getEmail());
            zaIzmenu.setIme(student.getIme());
            studentService.save(zaIzmenu);
            return new ResponseEntity<StudentDTO>(HttpStatus.OK);
    }
    return new ResponseEntity<StudentDTO>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/studijski-program/{korisnickoIme}")
    public ResponseEntity<StudijskiProgramDTO> getStudijskiProgram(@PathVariable("korisnickoIme") String korisnickoIme)
    {
        Optional<Student> student = studentService.findOne(korisnickoIme);

        StudijskiProgram studijskiProgram = student.get().getStudentNaGodini().getGodinaStudija().getStudijskiProgram();
        StudijskiProgramDTO studijskiProgramDTO = new StudijskiProgramDTO(studijskiProgram.getNaziv());
        return ResponseEntity.ok(studijskiProgramDTO);
    }

    @GetMapping("/trenutna-godina/{korisnickoIme}")
    public ResponseEntity<GodinaStudijaDTO> getTrenutnaGodina(@PathVariable("korisnickoIme") String korisnickoIme) {
        try {
            Optional<Student> studentOptional = studentService.findOne(korisnickoIme);
    
            if (studentOptional.isEmpty()) {
                return ResponseEntity.notFound().build();
            }
    
            Student student = studentOptional.get();
            if (student.getStudentNaGodini() == null || student.getStudentNaGodini().getGodinaStudija() == null) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
    
            GodinaStudija godinaStudija = student.getStudentNaGodini().getGodinaStudija();
            GodinaStudijaDTO godinaStudijaDTO = new GodinaStudijaDTO(godinaStudija.getGodina());
            return ResponseEntity.ok(godinaStudijaDTO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }



    
    @GetMapping("/nastavnik/studenti")
    public ResponseEntity<List<StudentDTO>> dobaviStudente(){
        Iterable<Student> studenti = studentService.findAll();
        ArrayList<StudentDTO> studentiDTO = new ArrayList<>();
        for(Student student : studenti){
            StudentNaGodini studentNaGodini = student.getStudentNaGodini();
            Korisnik korisnik = korisnikService.findOne(student.getId()).get();
            studentiDTO.add(new StudentDTO(student.getIme(), studentNaGodini.getBrojIndeksa(), studentNaGodini.getDatumUpisa(), student.getUsername()));
        }
        return new ResponseEntity<>(studentiDTO, HttpStatus.OK);
    }
    

    @GetMapping("/{korisnickoIme}/predmeti")
    public ResponseEntity<List<PredmetDTO>> getCoursesForStudent(@PathVariable String korisnickoIme) {
        Student student = studentService.findOne(korisnickoIme).orElse(null);

        if (student == null) {
            return ResponseEntity.notFound().build();
        }
        StudentNaGodini studentNaGodini = student.getStudentNaGodini();

        if (studentNaGodini == null) {
            return ResponseEntity.notFound().build();
        }
        GodinaStudija godinaStudija = studentNaGodini.getGodinaStudija();

        if (godinaStudija == null) {
            return ResponseEntity.notFound().build();
        }

        List<Predmet> predmeti = godinaStudija.getPredmeti();

        ArrayList<PredmetDTO> predmetiDTO = new ArrayList<>();
        for(Predmet predmet : predmeti)
        {
            predmetiDTO.add(new PredmetDTO(predmet.getId(), predmet.getNaziv(), predmet.getEspb(), predmet.getObavezan(), predmet.getBrojPredavanja(), predmet.getBrojVezbi(), predmet.getDrugiObliciNastave(), predmet.getIstrazivackiRad(), predmet.getOstaliCasovi()));
        }

        
        return ResponseEntity.ok(predmetiDTO);
    }


    @GetMapping("/{korisnickoIme}/polozeni")
    public ResponseEntity<List<PolozeniIspitiDTO>> getPolozeniIspiti(@PathVariable String korisnickoIme) {
        List<PolozeniIspitiDTO> polozeniIspiti = studentService.getPolozeniIspiti(korisnickoIme);
        return ResponseEntity.ok(polozeniIspiti);
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody Student student) {
        studentService.save(student);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        studentService.delete(id);
        return ResponseEntity.noContent().build();
    }
}

