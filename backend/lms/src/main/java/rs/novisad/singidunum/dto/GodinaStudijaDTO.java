package rs.novisad.singidunum.dto;

import java.time.LocalDateTime;

public class GodinaStudijaDTO {

    private Long id;
    private Long godina;
    private Long studijskiProgramId;

    public GodinaStudijaDTO() {
        // Default constructor
    }



    public GodinaStudijaDTO(Long godina) {
        this.godina = godina;
    }
    public GodinaStudijaDTO(Long id, Long godina, Long studijskiProgramId) {
        this.id = id;
        this.godina = godina;
        this.studijskiProgramId = studijskiProgramId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStudijskiProgramId() {
        return studijskiProgramId;
    }

    public void setStudijskiProgramId(Long studijskiProgramId) {
        this.studijskiProgramId = studijskiProgramId;
    }

    @Override
    public String toString() {
        return "GodinaStudijaDTO [id=" + id + ", godina=" + godina + ", studijskiProgramId=" + studijskiProgramId + "]";
    }



    public Long getGodina() {
        return godina;
    }



    public void setGodina(Long godina) {
        this.godina = godina;
    }

}
