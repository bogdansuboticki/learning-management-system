package rs.novisad.singidunum.dto;




public class PolaganjeDTO {
private Long id;
private Long bodovi;
private String napomena;

    public PolaganjeDTO() {
        super();
    }

    public PolaganjeDTO(Long id, Long bodovi, String napomena) {
        this.id = id;
        this.bodovi = bodovi;
        this.napomena = napomena;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBodovi() {
        return bodovi;
    }

    public void setBodovi(Long bodovi) {
        this.bodovi = bodovi;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }

}

