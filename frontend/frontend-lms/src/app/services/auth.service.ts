import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { jwtDecode } from 'jwt-decode';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private baseUrl = 'http://localhost:8080/api';

  constructor(private http: HttpClient) { }

login(username: string, password: string): Observable<boolean> {
    return this.http.post(`${this.baseUrl}/login`, { username, password }, { responseType: 'text' })
      .pipe(
        map(jwt => {
          localStorage.setItem('currentUser', jwt);
          return true;
        })
      );
  }


  register(korisnickoIme: string ,password: string, email:string, ime:string, jmbg:string, userType:string, biografija: string ): Observable<boolean> {
    console.log("ROLE IS: "+userType)
    return this.http.post(`${this.baseUrl}/registracija`, { korisnickoIme, password, email, ime, jmbg, userType, biografija }, { responseType: 'text' })
      .pipe(
        map(response => {
          console.log(response);
          return true;
        })
      );
  }


  logout(): void {
    localStorage.removeItem('currentUser');
  }

  isAuthenticated(): boolean {
    return !!localStorage.getItem('currentUser');
  }


  getUseRole() {
    const token = localStorage.getItem('currentUser');
    if (token) {
      const decodedToken: any = jwtDecode(token);
      const roles = decodedToken.role;
      if (roles && roles.length > 0) {
        return roles[0].authority;
      }
      return null;
    }
  }


  getUsername() {
    const token = localStorage.getItem('currentUser');
    if (token) {

      try {
        const decodedToken: any = jwtDecode(token);

        const username = decodedToken.username;
        console.log("Ovde treba user: " + username);

        if (username) {
          console.log("Username: " + username);
          return username;
        }
      } catch (error) {
        console.error("Error decoding token:", error);
      }
    }

    return null;

  }
}
