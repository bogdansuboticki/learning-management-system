import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../model/student';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
    private apiUrl = 'http://localhost:8080/api/studenti';

  constructor(private http: HttpClient) { }

  getStudentByUsername(username: string): Observable<Student> {
    const url = `${this.apiUrl}/${username}`;
    return this.http.get<Student>(url);
  }

  updateStudent(username: string, student: Student): Observable<void> {
    const url = `${this.apiUrl}/${username}`;
    return this.http.put<void>(url, student);
  }
}
