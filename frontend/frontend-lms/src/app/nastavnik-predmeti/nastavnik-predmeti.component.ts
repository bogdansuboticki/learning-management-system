import { Component } from '@angular/core';
import { Predmet } from '../model/predmet';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nastavnik-predmeti',
  templateUrl: './nastavnik-predmeti.component.html',
  styleUrl: './nastavnik-predmeti.component.css'
})
export class NastavnikPredmetiComponent {

  predmeti: Predmet[] = [];
  private baseUrl = 'http://localhost:8080/api/nastavnici'
 
  constructor(private http: HttpClient, private authService : AuthService, private router: Router) {
   
  }
 
  ngOnInit(): void {
 
    this.http.get<Predmet[]>(`${this.baseUrl}/${this.authService.getUsername()}/predmeti`)
      .subscribe(data => {
        this.predmeti = data;
      });
  }

}
