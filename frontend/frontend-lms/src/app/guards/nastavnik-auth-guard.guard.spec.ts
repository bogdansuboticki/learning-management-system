import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { NastavnikAuthGuard } from './nastavnik-auth-guard.guard';

describe('nastavnikAuthGuardGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => NastavnikAuthGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
