import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { studentskaSluzbaAuthGuard } from './studentska-sluzba-auth.guard';

describe('studentskaSluzbaAuthGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => studentskaSluzbaAuthGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
