import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { StudentAuthGuard } from './student-auth-guard.guard';

describe('studentAuthGuardGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => StudentAuthGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
