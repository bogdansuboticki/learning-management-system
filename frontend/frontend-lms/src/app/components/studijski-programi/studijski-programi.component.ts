import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-studijski-programi',
  templateUrl: './studijski-programi.component.html',
  styleUrls: ['./studijski-programi.component.css']
})
export class StudijskiProgramiComponent implements OnInit {
  poslovna_ekonomija: string = `<p>
  Studijski program <b>Poslovna ekonomija</b> namenjen je studentima koji imaju želju i potrebu da se na kvalitetan način obrazuju i steknu znanja i veštine koje će im omogućiti da ostvare aktivnu ulogu u savremenom poslovnom svetu. Tokom studija, kroz izučavanje obaveznih i izbornih predmeta, studentima je omogućeno da stiču znanja iz oblasti poslovne ekonomije, kao i da se usmeravaju ka oblastima menadžmenta, finansija, računovodstva, marketinga, revizije i upravljanja ljudskim resursima. U cenu školarine su uključeni udžbenici.
</p>
<p>
  Ovaj program za cilj ima sticanje teorijsko-metodoloških i praktičnih ekonomskih znanja u oblasti poslovne ekonomije za kreativno rešavanje problema i donošenje strategijsko-operativnih odluka u poslovanju različitih privrednih organizacija, javnog sektora i neprofitnih organizacija, u globalnom digitalnom okruženju. Prateći zahteve tržišta rada i privrednog razvoja, cilj ovog studijskog programa je da studenti steknu neophodna funkcionalna znanja o načinu rada i strukturi poslovnih organizacija, kako bi na adekvatan način mogli da obavljaju složenije poslove unutar organizacija, kao i da koordiniraju i upravljaju razvojem istih na globalnom tržištu.
</p>
<p>
  Ključni aspekt nastave na ovom studijskom programu čini upotreba case study metodologije, koja studentima omogućava da, kroz analize studija slučaja uspešnih preduzeća iz naše zemlje i sveta, razvijaju sistematičnost i analitičnost u rešavanju problema. Na časovima vežbi iz primenjenih ekonomskih predmeta koriste se napredni softveri. Kontinuirani rad studenata na sticanju stručnih poslovnih veština (hard skills) i ličnih poslovnih veština (soft skills), čini da steknu pun spektar sposobnosti neophodnih za razvoj uspešne karijere. Još jedna prednost modernih studijskih programa je mogućnost učenja dva strana jezika od kojih je engleski obavezan, a drugi strani jezik je izborni: španski, grčki, ruski, francuski, nemački i italijanski.
</p>
<p>
  Nakon završetka studijskog programa Poslovna ekonomija studenti će na osnovu stečenih znanja, kompetencija i veština imati široke mogućnosti za zapošljavanje na pozicijama koje uključuju:
</p>
<ul>
  <li>upravljanje ključnim poslovnim funkcijama (menadžment, finansije, računovodstvo i marketing) u privrednim društvima i drugim organizacijama;</li>
  <li>upravljanje promenama i krizom u organizacijama;</li>
  <li>primenu metodologije računovodstva u cilju izveštavanja organa upravljanja i drugih korisnika finansijskih izveštaja, kao i u cilju prevencije krivičnih dela u oblasti privrede i prevencije ranjivosti organizacije na ova krivična dela;</li>
  <li>upotrebe alternativnih tehnika analize finansijskih izveštaja i poslovnih finansija u cilju utvrđivanja finansijske strukture i uspeha za potrebe poslovnih kombinacija, odnosno spajanja i pripajanja;</li>
  <li>korišćenja savremenih informacionih sistema u oblasti računovodstva i metodologije elektronskog poslovanja u cilju efikasnijeg ispunjavanja ciljeva organizacije.</li>
</ul>
<p>
  Nakon uspešnog završetka studija, studenti programa Poslovna ekonomija stiču akademsko zvanje diplomirani ekonomista.
</p>
<p>
  Mogućnosti za zaposlenje su brojne i obuhvataju poslove kao što su:
</p>
<ul>
  <li>poslovi ekonomiste u preduzećima javnog i i privatnog sektora svih profila delatnosti, u preduzećima javnog i i privatnog sektora svih profila delatnosti: poslovi planiranja, organizovanja, vođenja i kontrole poslovnih procesa; poslovi organizovanja strukture i procesa; poslovi merenja performansi;</li>
  <li>poslovi organizacionog razvoja, restrukturiranja, poslovnog konsaltinga;</li>
  <li>poslovi iz domena finansijskog i upravljačkog računovodstva u privrednim i neprivrednim organizacijama;</li>
  <li>donošenje finansijskih i investicionih odluka;</li>
  <li>poslovi finansijsko-računovodstvene analize;</li>
  <li>poslovi finansijskih stručnjaka;</li>
  <li>poslovi ekonomiste u banci;</li>
  <li>poslovi ekonomiste u marketing odeljenjima korporacija ili marketinškim agencijama.</li>
</ul>
<p>
  Na sajtu upisa osnovih studija možete dobiti više informacija o studijskom programu i predmetima. Putem ovog sajta takođe možete obaviti i pretprijavu za upis na studijski program.
</p>
`;


turizam_hotelijerstvo: string = `<p>Univerzitet Singidunum je vodeća visokoškolska institucija u regionu za obrazovanje stručnih kadrova iz oblasti turizma i hotelijerstva. Četvorogodišnje studije na Fakultetu za turistički i hotelijerski menadžment osmišljene su u skladu sa potrebama onih koji su se opredelili da razvijaju karijeru u oblasti turizma - trenutno najmasovnijoj i najbrže rastućoj industriji u svetu.</p>
<p>Nastava na ovom studijskom programu se zasniva na multidisciplinarnom pristupu. Pritom, posebno se obraća pažnja na znanja iz oblasti ekonomije, korišćenje informacionih tehnologija i na učenje svetskih jezika. Stručna praksa je sastavni deo studijskog programa.</p>
<p>Tri izborne opcije - Turizam, Hotelijerstvo ili Ekonomija hrane, pružiće studentima mogućnost sticanja profesionalnih i ličnih sposobnosti, i uspešno će ih pripremiti za vodeće stručne pozicije u ovoj profesiji.</p>
<p>Završetkom studija na Fakultetu za turistički i hotelijerski menadžment stiče se zvanje diplomirani ekonomista.</p>
<p>Pored aktuelnih izbornih opcija vezanih za turizam i hotelijerstvo, na Univerzitetu Singidunum započeli smo sa obrazovanjem stručnjaka iz oblasti ekonomije hrane.</p>
<p>Na ovoj izbornoj opciji se, osim opšteobrazovnih predmeta iz oblasti ekonomije i turizma, izučavaju i uskostručni predmeti kao što su restoraterstvo, međunarodna gastronomija, tehnologija životnih namirnica, hrana i održivi razvoj, principi ishrane i rekreacije i sl.</p>
<p>Studenti se na smeru Ekonomija hrane upoznaju sa mikrobiološkim, hemijskim, fizičkim, senzornim kvalitetom hrane, uče o tehnološkim procesima proizvodnje namirnica, o njihovoj hranljivoj vrednosti, o sistemu kvaliteta hrane u ugostiteljskim objektima i trgovinskim lancima, o kulturnim i geografskim aspektima hrane, a osim toga stiču i neophodna znanja iz ekonomskih oblasti kako bi mogli uspešno da rukovode distribucijom i prodajom hrane.</p>
<p>Turistička privreda i prateće delatnosti, kao i druge organizacije u turizmu zapošljavaju veliki broj ljudi. Dugoročna predviđanja najavljuju dalji rast turističkih putovanja i potrošnje u svetu i u našoj zemlji što će omogućiti širok spektar poslova za sve koji su zainteresovani da se zaposle u ovoj oblasti. Poslovi koje diplomirani studenti mogu da obavljaju obuhvataju: poslovanje hotela, upravljanje spa i wellness centrima, ketering, upravljanje komercijalnim i socijalnim ugostiteljskim objektima, rad u turističkim agencijama i organizacijama, unapređenje kvaliteta usluga, organizaciju događaja, prodaju, konsultantske usluge, marketing, upravljanje ljudskim resursima, istraživanje tržišta, upravljanje organskim farmama, vinarijama, farmama meda, ribnjacima i brojne druge poslove. Stečeno zvanje omogućava i kvalitetno obavljanje odgovarajućih poslova u saobraćaju, trgovini, bankarstvu, osiguranju i brojnim drugim područjima privatnog i javnog sektora. Ako iskoriste mogućnosti koje im se nude, diplomci sa Fakulteta za turizam i hotelijerstvo će sa sobom poneti znanje koji će ih izdvojiti iz mase onih koji se prijavljuju na tržište rada svake godine.</p>
<p>U cenu školarine su uključeni udžbenici za tekući semestar.</p>
`;


ekonomija_hrane: string = `<p>
Pored aktuelnih izbornih opcija vezanih za turizam i hotelijerstvo, na Univerzitetu Singidunum započeli smo sa obrazovanjem stručnjaka iz oblasti ekonomije hrane.
</p>
<p>
Na ovoj izbornoj opciji se, osim opšteobrazovnih predmeta iz oblasti ekonomije i turizma, izučavaju i uskostručni predmeti kao što su restoraterstvo, međunarodna gastronomija, tehnologija životnih namirnica, hrana i održivi razvoj, principi ishrane i rekreacije i sl.
</p>
<p>
Studenti se na smeru Ekonomija hrane upoznaju sa mikrobiološkim, hemijskim, fizičkim, senzornim kvalitetom hrane, uče o tehnološkim procesima proizvodnje namirnica, o njihovoj hranljivoj vrednosti, o sistemu kvaliteta hrane u ugostiteljskim objektima i trgovinskim lancima, o kulturnim i geografskim aspektima hrane, a osim toga stiču i neophodna znanja iz ekonomskih oblasti kako bi mogli uspešno da rukovode distribucijom i prodajom hrane.
</p>
`;

racunarske_nauke: string = `<p>
Svrha studijskog programa Računarske nauke jeste obrazovanje kompetentnih stručnjaka za zanimanja u oblasti računarskih nauka koja su tražena na domaćem i međunarodnom tržištu. Ovaj program, koji se više od jedne decenije realizuje i razvija na Univerzitetu Singidunum, rezultat je analize potreba tržišta i saradnje stručnjaka iz različitih domena računarstva. U toku studija stiču se teorijska i praktična upotreblјiva znanja koja omogućavaju brzo zaposlenje, nekada i pre kraja studija. Studenti imaju mogućnost usmerenja u dva prosperitetna domena, koja su sve više na ceni: bezbednost u kiber prostoru i programiranje i razvoj veb i mobilnih aplikacija.
</p>
<p>
Nakon četvorogodišnjih osnovnih akademskih studija, studenti poznaju principe i tehnike računarskih tehnologija, programiranja i računarske bezbednosti, sposobni su za rad sa više programskih jezika i računarskih platformi i imaju mogućnost da steknu svetski priznate sertifikate globalnih proizvođača (Microsoft, CISCO, CompTIA, EC-Council, Oracle, itd.). Dodatni industrijski IT sertifikati uz akademsku diplomu omogućavaju diplomiranim studentima da budu konkurentniji na tržištu rada.
</p>
<p>
Praktična nastava na svakom predmetu studijskog programa Računarske nauke ima za cilj da osposobi studente za praktično (eng. hands-on) iskustvo u radu sa izabranom tehnologijom. Već od prve godine studija, studenti mogu da se uključe u tzv. vannastavne projektne aktivnosti, gde se pod mentorstvom profesora Univerziteta Singidunum uključuju u implementaciju realnih IT projekata iz domena programiranja i razvoja aplikacija i računarske bezbednosti. Cilj je da nakon završenih studija student aplicira na tržište rada sa ekvivalentnim praktičnim znanjem koje je uporedivo sa višegodišnjim radom u računarskoj industriji.
</p>
<p>
Jedna od važnijih prednosti studijskog programa Računarske nauke je globalna prepoznatljivost diplome, zato što je nastavni program u potpunosti usaglašen sa preporukama jedinog svetskog standarda datog od strane The Joint Task Force on Computing Curricula, IEEE Computer Society and Association for Computing Machinery (ACM) u dokumentima Computer Science Curricula 2013 i Cybersecurity Curricula 2017.
</p>
<p>
Studijski program Računarske nauke obezbediće svim kandidatima mogućnost da u vrhunskim uslovima prate nastavu, uče od vodećih stručnjaka iz oblasti računarstva i steknu savremena, funkcionalna znanja za uspešnu karijeru u domaćim i međunarodnim IT kompanijama i obrazovnim institucijama. Nakon završetka i tokom završnih godina ovog studijskog programa, studentima se otvaraju široke mogućnosti za zapošlјavanje u okviru lepeze najtraženijih poslova, kao što su poslovi iz domena bezbednosti računarskih sistema, poslovi programera koji rade sa heterogenim tehnologijama, platformama i alatima, poslovi na održavanju računarskih sistema (administratori sistema), poslovi implementacije, održavanja i administracije sistema baza podataka i sl.
</p>
`;

informacione_tehnologije: string = `<p>
Studijski program Informacione tehnologije je usaglašen sa dostignutim stepenom razvoja informacionih tehnologija, savremenih veb servisa, internet marketinga, savremenih menadžerskih pristupa i u skladu je sa zahtevima koji proizilaze iz praktične primene saznanja iz ovih oblasti u poslovnim sistemima. Glavna odlika studijskog programa Informacione tehnologije je posvećivanje pažnje internet tehnologijama i veb servisima što je u skladu sa brzim tehnološkim promenama u ovoj oblasti.
</p>
<p>
Studijski program omogućava sticanje znanja neophodnih za rešavanje realnih problema koja se odnose na razumevanje, instalaciju, projektovanje, nabavku, prodaju i organizaciju IT poslova jedne organizacije. Osim toga, studijski program pruža i neophodna znanja iz oblasti ekonomije kako bi student bio osposobljen za razumevanje poslovnih sistema. Stečena znanja predstavljaju osnovu za primenu baza podataka i informacionih sistema kao podrške poslovanju. Pored toga, uz znanja projektovanja i izrade savremenih veb sajtova i uz znanja na koji način funkcionišu društvene (socijalne) mreže i “cloud” računarstvo student je pripremljen da na najbolji način organizuje Internet marketing i da na savremen način unapredi poslovanje organizacije. Posebnu vrednost programa predstavlja izučavanje praktičnih implementacija u računarskim aplikacijama koje se odnose na elektronsko poslovanje, analitiku i izveštavanje.
</p>
<p>
Proizvod ovog studijskog programa čine stručnjaci koji su u stanju da podrže poslovanje organizacije, odnosno odluke koje se zasnivaju na korišćenju svih raspoloživih i ažurnih informacija. Osnovu za to čini intenzivna analiza podataka, upotreba sistema za podršku u odlučivanju, poslovno izveštavanje i ostali principi koji se u poslednje vreme grupišu pod terminom “big data”.
</p>
<p>
Nakon završetka studijskog programa INFORMACIONE TEHNOLOGIJE otvaraju se široke mogućnosti za zapošljavanje na poslovima kao što su IT podrška, izrada veb sajtova, markentiška kampanja preko Interneta, zaštita podataka organizacije, projektovanje kompletnih informacionih sistema, nabavka, instalacija i prodaja IT proizvoda. Studijski program je koncipiran u skladu sa iskazanim potrebama većeg broja firmi koje posluju u Srbiji, a sa kojima Univerzitet Singidunum ima ugovore o poslovnoj saradnji.
</p>
`;


sii: string = `<p>
Ovaj studijski program obezbeđuje najšira znanja iz oblasti softverskog i informacionog inženjerstva. Na studijama se izučavaju metodološki aspekti razvoja složenih softverskih i informacionih sistema i najsavremenije prateće, posebno softverske tehnologije za primenu softverskog i informacionog inženjerstva u različitim domenskim oblastima.
</p>
<p>
Diplomirani inženjeri softverskog i informacionog inženjerstva su kompetentni da rešavaju realne probleme iz prakse i da obavljaju poslove iz oblasti:
</p>
<ul>
<li>Inženjer za programiranje veb aplikacije</li>
<li>Inženjer za programiranje mobilnih sistema</li>
<li>Inženjer za programiranje poslovnih sistema</li>
<li>Inženjer za veb-dizajn</li>
<li>Inženjer za razvoj i implementaciju zaštite u računarskim sistemima</li>
<li>Inženjer za razvoj softverskih modula elektronskog poslovanja</li>
</ul>
<p>
U cenu školarine su uključeni udžbenici za tekući semestar.
</p>
`;

fizicko: string = `<p>
Svrha ovog studijskog programa je da obezbedi savremen i kvalitetan proces obrazovanja novih generacija studenata, koji će posle uspešno završenih studija kompetentno i odgovorno obavljati zadatke diplomiranog profesora fizičkog vaspitanja i sporta i čiji će profesionalni pedagoški rad u obrazovnim i sportskim organizacijama konstruktivno povezivati fizičko vaspitanje, sport i društvenu sredinu i uticati na ukupan razvoj fizičke kulture.
</p>
<p>
Savladavanjem ovog studijskog programa, studenti se osposobljavaju za profesionalno angažovanje na razvijanju i unapređivanju:
</p>
<ul>
<li>fizičke kulture u okviru formalnog sistema obrazovanja Republike Srbije, bilo da svoj profesionalni angažman ostvaruju u osnovnim i srednjim školama ili predškolskim ustanovama</li>
<li>masovnog sporta koji okuplja decu i omladinu, kako u urbanim, tako i u ruralnim sredinama</li>
<li>sportske rekreacije, programirane prema potrebama pojedinih uzrasnih i/ili profesionalnih kategorija stanovništva</li>
</ul>
<p>
Stručno-obrazovani i profesionalno profilisani diplomirani profesori fizičkog vaspitanja i sporta, imaju ključnu ulogu u promovisanju fizičke kulture i sporta u celini (vrhunskog, masovnog i rekreativnog), kao jedne od najznačajnijih društvenih, obrazovnih i ličnih vrednosti.
</p>
<p>
Kvalitetno obrazovanje diplomiranih profesora fizičkog vaspitanja posebno je značajno ako se ima u vidu trend promovisanja vrhunskih dostignuća u sportu. U tom smislu, potreban je veliki broj obrazovanih pedagoga koji će kroz adekvatan pedagoški rad sa decom i mladima, pre svega u nastavi fizičkog vaspitanja i sporta, uspostaviti potreban balans između školskog sporta i sve izraženijih težnji da se stvaraju vrhunski sportisti.
</p>
<p>
Studijski program Fizičko vaspitanje i sport, sudentima obezbeđuje sticanje najnovijih akademskih, praktičnih, naučnih i stručnih znanja iz oblasti fizičkog vaspitanja i sporta. Studijski program je celovit, sveobuhvatan i strukturiran u skladu sa savremenim tokovima i tendencijama u obrazovanju kadrova iz oblasti fizičkog vaspitanja i sporta, a usklađen je i sa programima srodnih visokoškolskih ustanova iz okvira evropskog obrazovnog prostora. Prilikom koncipiranja studijskog programa, vodilo se računa o principima Bolonjske deklaracije, o najnovijim trendovima u ovoj oblasti u inostranim visokoškolskim ustanovama, kao i o formalnoj i strukturnoj usklađenosti sa predmetno-specifičnim standardima za akreditaciju studijskih programa.
</p>
<p>
Osnovni cilj studijskog programa Fizičko vaspitanje i sport, jeste da obezbedi studentima da uspešnim savladavanjem programa steknu kvalifikaciju diplomiranog profesora fizičkog vaspitanja i sporta. Kompetencije, akademska znanja i specifične praktične veštine ove profesije omogućavaju: efikasan profesionalni rad i primenu naučnih i stručnih dostignuća u nastavi fizičkog vaspitanja u predškolskim ustanovama, osnovnim i srednjim školama; uspešno sprovođenje trenažnog procesa u sportskim klubovima i organizacijama, kao i korektivnog i rekreativnog vežbanja u cilju poboljšanja zdravlja; primenu rezultata naučnih i stručnih istraživanja za razvoj i praćenje efekata fizičkih aktivnosti u nastavi fizičkog vaspitanja i efekata telesnog vežbanja drugih osoba koje upražnjavaju fizičke aktivnosti. Cilj studijskog programa je i da diplomirani studenti izgrade osnovu za permanentno naučno, profesionalno i stručno usavršavanje u oblasti sporta i fizičkog vaspitanja.
</p>
<p>
Nakon uspešnog završetka studija, studenti stiču akademsko zvanje diplomirani profesor fizičkog vaspitanja i sporta.
</p>
<p>
U cenu školarine su uključeni udžbenici za tekući semestar.
</p>
`;


menadzment_sport: string = `<p>
Studijski program Menadžment u sportu, obezbeđuje studentima sticanje najnovijih teorijskih i praktičnih naučnih i stručnih znanja iz oblasti sportskog menadžmenta. Studijski program je celovit i sveobuhvatan, a strukturiran je u skladu sa savremenim tendencijama u obrazovanju kadrova u sportu i usklađen je sa programima inostranih visokoškolskih ustanova. Prilikom koncipiranja studijskog programa, vodilo se računa o principima Bolonjske deklaracije, o najnovijim trendovima u ovoj oblasti u inostranim visokoškolskim ustanovama, kao i o formalnoj i strukturnoj usklađenosti sa predmetnospecifičnim standardima za akreditaciju studijskih programa. Studijski program Menadžment u sportu, u okviru delatnosti visokog obrazovanja ima svrhu i ulogu da obrazuje studente za profesiju diplomirani menadžer. U skladu sa Zakonom o visokom obrazovanju, Zakonom o sportu, i sa odredbama Pravilnika o nomenklaturi sportskih zanimanja i zvanja, menadžer u sportu je zanimanje lica koja se bave poslovima upravljanja, organizovanja, koordinacije, planiranja i kontrole rada organizacije u oblasti sporta.
</p>
<p>
Prema čl. 40 ovog Pravilnika, zvanja u okviru zanimanja menadžer u sportu su:
</p>
<ul>
<li>operativni sportski menadžer</li>
<li>sportski menadžer</li>
<li>sportski menadžer specijalista</li>
</ul>
<p>
Poslovi i aktivnosti koje menadžeri u sportu mogu da obavljaju su: pripremanje programa i projekata u oblasti sporta i kontrola njihovog izvršenja; vođenje poslovanja upravnih i stručnih organa i tela u organizacijama u oblasti sporta; organizovanje i realizacija sistema takmičenja; planiranje i kontrola korišćenja objekata; organizovanje utakmica, susreta i takmičenja; marketinške aktivnosti i dr. Menadžeri se profesionalno bave i poslovima posredovanja i pregovaranja u vezi sa bilo kojim vidom angažovanja sportista, u okviru različitih sportskih grana.
</p>
<p>
Dakle, svrha studijskog programa Menadžment u sportu je kvalitetno i savremeno obrazovanje novih generacija menadžera u sportu, koji će kompetentno i efikasno implementirati stečena znanja u radu i poslovanju sportskih, ali i ostalih poslovnih organizacija, rukovoditi njihovim razvojnim planovima i programima sa ciljem njihovog uspešnog rada, pozitivnog doprinosa društvenoj zajednici i integrisanja u društveno okruženje. Izučavanjem naučnih disciplina u oblasti menadžmenta u sportu na ovom studijskom programu, menadžeri stiču znanja i kompetencije koje ih kvalifikuju za samostalan, profesionalan, individualni ili timski rad, u svim oblastima privrede, a posebno u sportskoj delatnosti.
</p>
<p>
Nakon uspešnog završetka studija, studenti stiču akademsko zvanje diplomirani menadžer u sportu.
</p>
<p>
U cenu školarine su uključeni udžbenici za tekući semestar.
</p>
`;


anglistika: string = `<p>
Studijski program Anglistika je nastao kao rezultat pažljivog osluškivanja potreba savremenog tržišta i težnje da se na te potrebe u potpunosti odgovori. Ovaj studijski program omogućava sticanje praktičnih i teorijskih znanja, kroz najsavremenije koncepte i upotrebu savremenih sredstava nastave.
</p>
<p>
Raznovrsnost kompetencija za koje osposobljava ovaj program čini posebnu vrednost studija Anglistike, a to znači da se jednaka pažnja posvećuje oblasti jezika, odnosno oblasti prevodilaštva, psihološko-pedagoško metodičkoj oblasti, kao i oblastima kulture i književnosti.
</p>
<p>
Savremen i posebno osmišljen koncept nastave, pruža studentima, ne samo mogućnost za dalja usavršavanja u mnogim lingvističkim i/ili srodnim pravcima, nego i sticanje višestruko primenjivih kompetencija, koje im, odmah po diplomiranju, omogućavaju zaposlenje u okviru širokog spektra delatnosti, počevši od nastave jezika, preko prevodilaštva, pa sve do rada u okviru oblasti u kojima postoji potreba za interkulturalnom komunikacijom, odnosno upotrebom znanja i veština koje studijski program Anglistika donosi; zapravo, gotovo da nema segmenta, bilo privatnog, bilo državnog sektora, gde sposobnosti, stečene na ovom studijskom programu, ne mogu pronaći svoju praktičnu primenu.
</p>
<p>
Na taj način, po završetku studijskog programa Anglistika, studenti postaju izuzetno kompetitivni na tržištu rada, kako u zemlji i regionu, tako i šire, budući da njihove sposobnosti nisu usmerene ka pojedinačnoj ili malom broju oblasti u vezi sa engleskim jezikom.
</p>
<p>
Sadržaj, struktura i način realizacije studijskog programa Anglistika, omogućiće svojim studentima sticanje znanja i veština koje u potpunosti odgovaraju savremenim zahtevima tržišta, odnosno – osposobiće studente za samostalan rad u okviru, najpre delatnosti nastave jezika, te prevodilaštva, ali i drugih delatnosti koje zahtevaju temeljno poznavanje Anglistike.
</p>
<p>
Nakon diplomiranja na ovom studijskom programu, diplomci se mogu zaposliti na sledećim radnim mestima:
</p>
<ul>
<li>Nastavnik/profesor engleskog jezika u osnovnim/srednjim školama</li>
<li>Predavač/nastavnik engleskog jezika u vrtićima, privatnim školama jezika</li>
<li>Predavač/nastavnik engleskog jezika u okviru instituta za jezik</li>
<li>Književni prevodilac</li>
<li>Stručni prevodilac (ekonomija, pravo, medicina, turizam i hotelijerstvo, informacione tehnologije, itd.)</li>
<li>Sudski tumač za engleski jezik</li>
<li>Simultani prevodilac</li>
<li>Konsekutivni prevodilac</li>
<li>Dopisnik (za novinske agencije)</li>
<li>Jezički savetnik/lektor stranog jezika</li>
</ul>
`;


zivotna_sredina: string = `<p>
Studijski program osnovnih akademskih studija Životna sredina i održivi razvoj, u četvorogodišnjem trajanju, ima za cilj da pitanja zaštite životne sredine, kao što su zagađenje, klimatske promene i očuvanje biodiverziteta, elaborira sa ekološkog, ali i sa ekonomskog stanovišta.
</p>
<p>
Program razvija sposobnosti razumevanja životne sredine i održivog razvoja i resursa ekonomije, definiše konceptualne osnove održivog razvoja i praktične alate analize, kvantifikacije i interpretacije indikatora stanja, očuvanja i unapređenja životne sredine.
</p>
<p>
Ovaj studijski program je proistekao iz potrebe da se oblikuju specifična znanja iz fundamentalnih nauka, ali i iz sfera ekonomije životne sredine i resursa, koje su danas nezaobilazni faktori interdisciplinarnog razumevanja fenomenologije oblasti životne sredine.
</p>
<p>
Studijski program predstavlja pravi izbor za one koji žele da se osposobe za razumevanje i kreiranje rešenja savremenih ekoloških problema, a ekonomska analiza je nezaobilazna, posebno danas, za utvrđivanje korisnosti, troškova i procene efikasnosti različitih instrumenata zaštite životne sredine i dizajniranje novih intervencija na putu ka održivom razvoju.
</p>
<p>
Jedinstvenost ovog studijskog programa je u tome što na optimalan način korespondiraju znanja i veštine iz fundamentalnih, pre svega prirodno-matematičkih, nauka sa znanjima i veštinama iz ključnih socio-ekonomskih oblasti.
</p>
<p>
Potrebe za stručnjacima u oblasti očuvanja životne sredine i održivog razvoja su rastuće Nakon diplomiranja na ovom studijskom programu, diplomci se mogu zaposliti u sledećim institucijama/organizacijama:
</p>
<ul>
<li>međunarodne organizacije za zaštitu prirode i životne sredine;</li>
<li>javne službe i preduzeća, lokalne samouprave, agencije i direkcije za zaštitu životne sredine, državne i gradske uprave, sekretarijati;</li>
<li>privreda i industrija: svaki vid proizvodnje i prerade podrazumeva angažovanje stručnjaka iz oblasti zaštite životne sredine;</li>
<li>turističke organizacije, organizacije upravljači zaštićenim prirodnim dobrima;</li>
<li>konsultantske kuće i agencije iz oblasti zaštite životne sredine;</li>
<li>organizacije civilnog društva (nevladine organizacije) u oblasti zaštite životne sredine;</li>
<li>preduzetnička delatnost i freelancing poslovanje u oblasti zaštite životne sredine (upravljanje ekoprojektima, ekoremedijacija, procene uticaja i rizika po životnu sredinu, studije izvodljivosti i opravdanosti, primena standarda i sertifikacija u zaštiti životne sredine, i slično).</li>
</ul>
<p>
Nakon uspešnog završetka studija, studenti stiču zvanje DIPLOMIRANI ANALITIČAR ZAŠTITE ŽIVOTNE SREDINE.
</p>
<p>
U cenu školarine su uključeni udžbenici za tekući semestar.
</p>
`;

farmacija: string = `<p>
Karijera u farmaciji je jedna od najprestižnijih karijera u svetu. Raznolikost karijernih puteva, koji postoje u farmaciji je jedan od razloga zašto upisati farmaciju. Od razvoja leka, kliničkog ispitivanja leka, do izdavanja leka odnosno pružanja farmaceutske zdravstvene usluge u apoteci, lako se može odabrati profesionalni put, koji Vas najviše zanima. Nakon završetka studijskog programa Farmacija stičete zvanje Magistar farmacije. Važno je imati na umu da ne morate odmah znati šta tačno želite da radite, jer posedovati diplomu Magistra farmacije je prestižno i otvara Vam vrata nebrojenim mogućnostima za obavljanje profesije farmaceuta.
</p>
<p>
Studijski program integrisanih osnovnih i master akademskih studija Farmacija traje pet godina i izvodi se u deset semestara. Obim studija izražen je ukupnim brojem od 300 ЕSPB bodova. Studijski program ima 37 obaveznih i 4 izborna predmeta, završni rad i obaveznu stručnu praksu. Stručna praksa se obavlja u zdravstvenim ustanovama-apotekama privatnog i državnog sektora, kao i u bolničkoj apoteci. Imaćete priliku u toku studija da posetite zdravstvene ustanove, naučno-istraživačke institucije, kao i kompanije iz oblasti farmaceutske industrije.
</p>
<p>
Studijski program integrisanih osnovnih i master akademskih studija Farmacija je interdisciplinaran i naučno orijentisan. Studijski program farmacija će Vam obezbediti znanja i veštine iz različitih oblasti. Posedovaćete znanja iz bazičnih prirodnih nauka, tj., iz hemije, biologije, fizike, kao i iz bazičnih medicinskih nauka odnosno anatomije, fiziologije, i dr., koje čine osnovu za sticanje znanja iz predmeta, koji predstavljaju temelj Farmacije, kao što su farmaceutska hemija, farmakognozija, farmaceutska tehnologija, farmakokinetika, farmakologija, farmakoterapija, klinička farmacija, analitika lekova, kozmetologija, dijetetika, i dr.
</p>
<p>
Posedovaćete, shodno navedenom, znanja i veštine, koje Vam omogućavaju rad u celokupnom spektru delatnosti, koje su vezane za lekove, kao i medicinska sredstva, dijetetske i kozmetičke proizvode, od formulacije i razvoja, proizvodnje, kontrole kvaliteta, registracije i marketinga lekova, medicinskih sredstava, dijetetskih i kozmetičkih proizvoda, uključujući i kliničko ispitivanje lekova, do dizajna individualne terapije za pacijente, izdavanja lekova, medicinskih sredstava i dijetetskih proizvoda pacijentu i pružanja informacija o njihovoj pravilnoj i bezbednoj primeni. Studijski program Farmacija pruža magistrima farmacije znanje i iz oblasti farmakoekonomije, menadžmenta i zakonske regulative, kako bi magistri farmacije bili osposobljeni za efikasno i dugoročno upravljanje u okviru apoteke, farmaceutske industrije, i dr.
</p>
<p>
Nakon uspešnog završetka studijskog programa Farmacija možete se zaposliti na sledećim radnim mestima:
</p>
<ul>
<li>u apoteci (javnog i bolničkog tipa) na poslovima nabavke, čuvanja, izrade i izdavanja lekova, medicinskih sredstava, dijetetskih i kozmetičkih proizvoda, pružanja informacija o njihovoj pravilnoj i bezbednoj primeni, kao i u drugim zdravstvenim ustanovama,</li>
<li>u privredi/farmaceutskoj industriji (formulacija i razvoj, proizvodnja, kontrola kvaliteta i registracija lekova, medicinskih sredstava, dijetetskih i kozmetičkih proizvoda),</li>
<li>u ustanovama, koje se bave kliničkim ispitivanjima lekova,</li>
<li>u oblasti marketinga i regulative u farmaceutskim firmama,</li>
<li>u veleprodaji lekova, medicinskih sredstava, dijetetskih i kozmetičkih proizvoda,</li>
<li>u regulatornim telima i ustanovama,</li>
<li>u naučnoistraživačkim institucijama, razvojnim i kontrolnim laboratorijama,</li>
<li>u obrazovnim institucijama,</li>
<li>osnivanje svog privatnog biznisa.</li>
</ul>
<p>
Studijski program Farmacija Vam pruža mogućnost daljeg usavršavanja u okviru specijalističkih i doktorskih akademskih studija, kao i u okviru različitih zdravstvenih specijalizacija.
</p>
`;

  sanitizedDescription: SafeHtml | null = null;

  programs = [
    { id: 1, name: 'Poslovna ekonomija', description: this.poslovna_ekonomija },
    { id: 2, name: 'Turizam i hotelijerstvo', description: this.turizam_hotelijerstvo },
    { id: 3, name: 'Ekonomija hrane', description: this.ekonomija_hrane },
    { id: 4, name: 'Računarske nauke', description: this.racunarske_nauke },
    { id: 5, name: 'Informacione tehnologije', description: this.informacione_tehnologije },
    { id: 6, name: 'Fizičko vaspitanje i sport', description: this.fizicko },
    { id: 7, name: 'Menadžment u sportu', description: this.menadzment_sport },
    { id: 8, name: 'Anglistika', description: this.anglistika },
    { id: 8, name: 'Životna sredina i održivi razvoj', description: this.zivotna_sredina },
    { id: 8, name: 'Farmacija', description: this.farmacija },
    { id: 8, name: 'Softversko i informaciono inženjerstvo', description: this.sii },
    // Add more programs as needed
  ];
  selectedProgram: any; // To store the selected program

  constructor(private sanitizer: DomSanitizer, private router: Router) { }

  ngOnInit(): void {
    this.sanitizedDescription = this.sanitizer.bypassSecurityTrustHtml(this.poslovna_ekonomija);
    this.sanitizedDescription = this.sanitizer.bypassSecurityTrustHtml(this.turizam_hotelijerstvo);
    this.sanitizedDescription = this.sanitizer.bypassSecurityTrustHtml(this.ekonomija_hrane);
    this.sanitizedDescription = this.sanitizer.bypassSecurityTrustHtml(this.racunarske_nauke);
    this.sanitizedDescription = this.sanitizer.bypassSecurityTrustHtml(this.informacione_tehnologije);
    this.sanitizedDescription = this.sanitizer.bypassSecurityTrustHtml(this.sii);
    this.sanitizedDescription = this.sanitizer.bypassSecurityTrustHtml(this.fizicko);
    this.sanitizedDescription = this.sanitizer.bypassSecurityTrustHtml(this.menadzment_sport);
    this.sanitizedDescription = this.sanitizer.bypassSecurityTrustHtml(this.anglistika);
    this.sanitizedDescription = this.sanitizer.bypassSecurityTrustHtml(this.zivotna_sredina);
    this.sanitizedDescription = this.sanitizer.bypassSecurityTrustHtml(this.farmacija);
    
  }

  selectProgram(program: any): void {
    this.selectedProgram = program;
  }



  navigateTo(route: string): void {
    this.router.navigateByUrl(route);
  }
}
