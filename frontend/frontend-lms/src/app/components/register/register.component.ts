import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})


export class RegisterComponent {
  username: string = '';
  password: string = '';
  ime: string = '';
  email: string = '';
  jmbg: string = '';
  biografija: string = '';
  role:string = '';


  constructor(private router: Router, private authService: AuthService) { }


  navigateTo(route: string): void {


    this.router.navigateByUrl(route);

  }

  toggleBiographyField(): void {

    const roleSelect = document.getElementById("role") as HTMLSelectElement;
    const biographyField = document.getElementById("biografijaField") as HTMLDivElement;
    
    if (roleSelect.value === "nastavnik") {
      biographyField.style.display = "block";
    } else {
      biographyField.style.display = "none";
    }
  }

  onSubmit() {
    const roleSelect = document.getElementById("role") as HTMLSelectElement;
    console.log(roleSelect.value)
    this.authService.register(this.username, this.password,  this.email, this.ime, this.jmbg, roleSelect.value ,this.biografija).subscribe(
      () => {
        this.router.navigate(['/dashboard']); 
      },
      (error) => {
        console.error('Registration error:', error);
      }

    );

  }

}
