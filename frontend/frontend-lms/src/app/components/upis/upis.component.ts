import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { GodinaStudija } from 'src/app/model/godinaStudija';
import { Korisnik } from 'src/app/model/korisnik';
import { Student } from 'src/app/model/student';
import { StudijskiProgram } from 'src/app/model/studijskiProgram';

@Component({
  selector: 'app-upis',
  standalone: true,
  imports: [CommonModule, FormsModule,        
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatSnackBarModule,],
  templateUrl: './upis.component.html',
  styleUrl: './upis.component.css'
})
export class UpisComponent implements OnInit{
  
  private studentiUrl = 'http://localhost:8080/api/studenti';
  
  private studijskiProgramiUrl = 'http://localhost:8080/api/studijski-program';
  private korisnikStudijskiProgramiUrl = 'http://localhost:8080/api/studenti/studijski-program';  
  private godineStudijaUrl = 'http://localhost:8080/api/godina-studija';
  private trenutnaGodinaStudija = 'http://localhost:8080/api/studenti/trenutna-godina';
  studenti: string[] = [];
  studijskiProgrami: string[] = [];
  godineStudija: number[]= [];

  trenutnaGodina: number = 0;

  selectedStudentUsername: string = '';
  selectedStudijskiProgram: string = '';
  selectedGodinaStudija: string = '';

  constructor(private http: HttpClient, private router: Router, private snackBar: MatSnackBar) {
    
  }

  onStudentSelect() {
    console.log('Selected student:', this.selectedStudentUsername);

  this.http.get<StudijskiProgram>(`${this.korisnikStudijskiProgramiUrl}/${this.selectedStudentUsername}`)
  .subscribe(data => {
    if (data) {
      this.selectedStudijskiProgram = data.naziv;

      
  this.http.get<GodinaStudija[]>(`${this.godineStudijaUrl}/${this.selectedStudijskiProgram}`)
  .subscribe(data => {
    this.godineStudija = data.map(godina => godina.godina);
});   
    } else {
      console.log('No data returned.');
    }
  }, error => {
    console.error('Error fetching data:', error);
  });


  this.http.get<GodinaStudija>(`${this.trenutnaGodinaStudija}/${this.selectedStudentUsername}`)
  .subscribe(data => {
    if (data) {
      this.trenutnaGodina = data.godina;
    } else {
      console.log('No data returned.');
    }
  }, error => {
    console.error('Error fetching data:', error);
  });






  }

  onStudijskiProgramSelect() {
    console.log('Selected studijski program:', this.selectedStudijskiProgram);
    this.http.get<GodinaStudija[]>(`${this.godineStudijaUrl}/${this.selectedStudijskiProgram}`)
    .subscribe(data => {
      this.godineStudija = data.map(godina => godina.godina);
  });   
  }

  onGodinaStudijaSelect() {
    console.log('Selected godina studija:', this.selectedGodinaStudija);
  }

  ngOnInit(): void {

    this.http.get<Student[]>(`${this.studentiUrl}`)
      .subscribe(data => {
        this.studenti = data.map(korisnickoIme => korisnickoIme.korisnickoIme);
      });


      this.http.get<GodinaStudija[]>(`${this.godineStudijaUrl}`)
        .subscribe(data => {
          this.godineStudija = data.map(godina => godina.godina);
      });   
      
      this.http.get<StudijskiProgram[]>(`${this.studijskiProgramiUrl}`)
      .subscribe(data => {
        this.studijskiProgrami = data.map(program => program.naziv);
      });
  }

  submit(korisnickoIme: string, studijskiProgram: string, godinaStudija: string) {
    this.http.put(`http://localhost:8080/api/studentska-sluzba/studenti/upisi/${korisnickoIme}`, null, {
      params: {
        godina: godinaStudija,
        studijskiProgramNaziv: studijskiProgram
      }
    }).subscribe(() => {
      console.log("Student successfully enrolled.");
      this.snackBar.open('Student uspesno upisan', 'Zatvori', {
        duration: 3000, // Duration in milliseconds
      });
      this.router.navigate(['/studentska-sluzba/uloge']);
    }, error => {
      console.error('Error enrolling student:', error);
      // Handle error, e.g., display an error message
      this.snackBar.open('Error enrolling student', 'Zatvori', {
        duration: 3000, // Duration in milliseconds
      });
    });
  }
  
  

  
  
}
