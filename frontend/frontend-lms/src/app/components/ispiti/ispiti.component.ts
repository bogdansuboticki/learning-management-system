import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Ispit } from 'src/app/model/ispit';
import { Predmet } from 'src/app/model/predmet';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-ispiti',
  standalone: true,
  imports: [
    CommonModule
  ],
  templateUrl: './ispiti.component.html',
  styleUrl: './ispiti.component.css'
})
export class IspitiComponent {

  ispiti: Ispit[] = [];
  private baseUrl = 'http://localhost:8080/api/studenti'

  constructor(private http: HttpClient, private authService : AuthService) {
    
  }

  ngOnInit(): void {

    this.http.get<Ispit[]>(`${this.baseUrl}/${this.authService.getUsername()}/polozeni`)
      .subscribe(data => {
        this.ispiti = data;
      });
  }
}
