import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { StudentService } from 'src/app/services/student.service';
import { Student } from 'src/app/model/student';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  student: Student = new Student('','', '', ''); 
  loggedInUsername: string = ''; 

  constructor(private authService: AuthService, private studentService: StudentService) { }

  ngOnInit(): void {
    this.loggedInUsername = this.authService.getUsername();
    this.studentService.getStudentByUsername(this.loggedInUsername).subscribe(
      (response) => {
        this.student = response;
      },
      (error) => {
        console.error('Error fetching student data:', error);
      }
    );
  }

  updateStudent(): void {
    this.studentService.updateStudent(this.loggedInUsername, this.student).subscribe(
      () => {
        console.log('Student izmenjen uspesno');
      },
      (error) => {
        console.error('Greska u izmeni studenta:', error);
      }
    );
  }
}
