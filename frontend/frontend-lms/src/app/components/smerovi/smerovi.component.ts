import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { StudijskiProgram } from 'src/app/model/studijskiProgram';
import { DeleteConfirmationDialogComponent } from '../delete-confirmation-dialog/delete-confirmation-dialog.component';

@Component({
  selector: 'app-smerovi',
  templateUrl: './smerovi.component.html',
  styleUrls: ['./smerovi.component.css']
})
export class SmeroviComponent implements OnInit {
  checkedRows: boolean[] = [];
  smerovi: any[] = [];
  fakulteti: any[] = [];
  newStudijskiProgram: any = { naziv: '', fakultet: '', brojGodina: '' };
  smeroviZaObrisati: StudijskiProgram[] = []

  constructor(private http: HttpClient, private dialog: MatDialog, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.checkedRows = new Array(this.smerovi.length).fill(false);
    this.http.get<any>('http://localhost:8080/api/studijski-program/svi').subscribe(data => {
      this.smerovi = data;
    });

    this.http.get<any>('http://localhost:8080/api/fakultet').subscribe(data => {
      this.fakulteti = data;
    });
  }

  submitNewProgram(): void {
    this.http.post(`http://localhost:8080/api/studentska-sluzba/studijski-program`, null, {
      params: {
        naziv: this.newStudijskiProgram.naziv,
        nazivFakulteta: this.newStudijskiProgram.fakultet,
        godine: this.newStudijskiProgram.brojGodina
      }
    }).subscribe(() => {
      console.log("Student successfully enrolled.");
      this.snackBar.open('Studijski program uspesno dodat', 'Zatvori', {
        duration: 3000, // Duration in milliseconds
      });
      this.ngOnInit();
      
      // Reset the form inputs
      this.newStudijskiProgram = {};
    }, error => {
      console.error('Error enrolling student:', error);
      // Handle error, e.g., display an error message
      this.snackBar.open('Error enrolling student', 'Zatvori', {
        duration: 3000, // Duration in milliseconds
      });
    });
  }
  

  openConfirmationDialog() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '300px',
      data: this.newStudijskiProgram
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.submitNewProgram(); // Submit the new program
      }
    });
  }



  deleteCheckedRows(): void {
    this.smeroviZaObrisati = this.smerovi.filter((_, index) => this.checkedRows[index]);
  
    const dialogRef = this.dialog.open(DeleteConfirmationDialogComponent, {
      width: '300px'
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.smeroviZaObrisati.forEach(smer => {
          this.http.delete(`http://localhost:8080/api/studentska-sluzba/studijski-program/${smer.naziv}`).subscribe(
            () => {
              console.log('Study program deleted successfully');
              this.snackBar.open('Uspesno obrisano', 'Zatvori', {
                duration: 3000,
              });
              this.ngOnInit();
            },
            error => {
              this.snackBar.open('Greska! Neuspesno brisanje', 'Zatvori', {
                duration: 3000,
              });
              console.error('Error deleting study program:', error);
            }
          );
        });
      }
    });
  
    // Clear the checkedRows array
    this.checkedRows = [];
  }
}
