import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  constructor(private router: Router, private authService : AuthService) {}
 
  ngOnInit() {
    const userRole = this.authService.getUseRole();
    console.log("Ovde ide user role" + userRole)
    switch (userRole) {
      case 'ROLE_ADMIN':
        this.router.navigate(['/studentska-sluzba']);
        break;
      case 'ROLE_NASTAVNIK':
        this.router.navigate(['/nastavnik']);
        break;
      case 'ROLE_STUDENT':
        this.router.navigate(['/student']);
        break;
      default:
        break;
    }
  }
 

}
