import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PravaPristupa } from 'src/app/model/pravaPristupa';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-uloge',
  templateUrl: './uloge.component.html',
  styleUrl: './uloge.component.css'
})
export class UlogeComponent implements OnInit{

  korisnici: PravaPristupa[] = [];
  private baseUrl = 'http://localhost:8080/api/studentska-sluzba'

  constructor(private http: HttpClient, private authService : AuthService, private router: Router) {
    
  }

  ngOnInit(): void {

    this.http.get<PravaPristupa[]>(`${this.baseUrl}/korisnici`)
      .subscribe(data => {
        this.korisnici = data;
      });
  }

}
