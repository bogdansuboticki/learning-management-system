import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UredjivanjeUlogeComponent } from './uredjivanje-uloge.component';

describe('UredjivanjeUlogeComponent', () => {
  let component: UredjivanjeUlogeComponent;
  let fixture: ComponentFixture<UredjivanjeUlogeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UredjivanjeUlogeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(UredjivanjeUlogeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
