import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-uredjivanje-uloge',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule
  ],
  templateUrl: './uredjivanje-uloge.component.html',
  styleUrl: './uredjivanje-uloge.component.css'
})
export class UredjivanjeUlogeComponent implements OnInit {

  private baseUrl = 'http://localhost:8080/api/studentska-sluzba/korisnici'
  korisnickoIme: string | null = '';
  selectedRole: string = "NO_ROLE";

  constructor(private route: ActivatedRoute, private cdr: ChangeDetectorRef, private http: HttpClient, private router: Router, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.korisnickoIme = params.get('korisnickoIme');
      this.cdr.detectChanges(); // Manually trigger change detection
      console.log('KORISNICKO IME:', this.korisnickoIme);
    });
  }

  izmeni() {
    this.http.put(`${this.baseUrl}/${this.korisnickoIme}?pravoPristupaNaziv=${this.selectedRole}`, {}, { responseType: 'text' })
    .subscribe(response => {
      this.snackBar.open('Uloga uspesno promenjena', 'Zatvori', {
        duration: 3000, // Duration in milliseconds
      });
      this.router.navigate(['/studentska-sluzba/uloge']); 
    });
  }

}
