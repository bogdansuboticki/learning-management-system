import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NastavnikStudentPodaciComponent } from './nastavnik-student-podaci.component';

describe('NastavnikStudentPodaciComponent', () => {
  let component: NastavnikStudentPodaciComponent;
  let fixture: ComponentFixture<NastavnikStudentPodaciComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NastavnikStudentPodaciComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NastavnikStudentPodaciComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
