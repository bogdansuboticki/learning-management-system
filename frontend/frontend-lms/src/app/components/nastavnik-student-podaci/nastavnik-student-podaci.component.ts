import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { polozeniIspiti } from 'src/app/model/polozeniIspiti';
import { Student } from 'src/app/model/student';
import { studentPodaci } from 'src/app/model/studentPodaci';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-nastavnik-student-podaci',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './nastavnik-student-podaci.component.html',
  styleUrl: './nastavnik-student-podaci.component.css'
})
export class NastavnikStudentPodaciComponent {

  korisnickoIme: string = '';
  studentPodaci: studentPodaci = {ime:'', jmbg: '', brojIndeksa: '', polozeni: []  };
  polozeniIspiti: polozeniIspiti[] =[];
  private baseUrl = 'http://localhost:8080/api/nastavnici'
  
 
  constructor(private http: HttpClient, private authService : AuthService, private router: Router, private route : ActivatedRoute) {
   
  }
 
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.korisnickoIme = params['korisnickoIme'];
      console.log('Smer naziv:', this.korisnickoIme);
      
    });
 
    this.http.get<studentPodaci>(`${this.baseUrl}/nastavnik-student-podaci/${this.korisnickoIme}`)
      .subscribe(data => {
        this.studentPodaci = data;
        this.polozeniIspiti = data.polozeni;
        
      });
  }

}
