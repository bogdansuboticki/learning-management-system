import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentPrijavaispitaComponent } from './student-prijavaispita.component';

describe('StudentPrijavaispitaComponent', () => {
  let component: StudentPrijavaispitaComponent;
  let fixture: ComponentFixture<StudentPrijavaispitaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StudentPrijavaispitaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StudentPrijavaispitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
