import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { CommonModule } from '@angular/common';
import { Prijava } from 'src/app/model/prijava';


@Component({
  selector: 'app-student-prijavaispita',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './student-prijavaispita.component.html',
  styleUrls: ['./student-prijavaispita.component.css']
})
export class StudentPrijavaispitaComponent {

  prijave: Prijava[] = [];
  private baseUrl = 'http://localhost:8080/api/studenti';

  constructor(private http: HttpClient, private authService: AuthService) {}

  ngOnInit(): void {
    this.loadPrijave();
  }

  loadPrijave(): void {
    this.http.get<Prijava[]>(`${this.baseUrl}/${this.authService.getUsername()}/prijavljeni-ispiti`)
      .subscribe(data => {
        console.log(data);
        this.prijave = data;
      });
  }

  prijaviIspit(evaluacijaZnanjaId: number, predmetId: number, username: string): void {
    const body = new URLSearchParams();
    body.set('evaluacijaZnanjaId', evaluacijaZnanjaId.toString());
    body.set('predmetId', predmetId.toString());
    body.set('username', username);
  
    this.http.post<any>(`${this.baseUrl}/prijavi-ispit`, body.toString(), {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    }).subscribe(response => {
      console.log(response);
      this.loadPrijave();
    }, error => {
      console.error('Error signing up for exam:', error);
    });
  }
}
