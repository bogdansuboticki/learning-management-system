import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Predmet } from 'src/app/model/predmet';
import { AuthService } from 'src/app/services/auth.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-predmeti',
  standalone: true,
  templateUrl: './predmeti.component.html',
  styleUrl: './predmeti.component.css',
  imports: [
    CommonModule
  ]
})
export class PredmetiComponent implements OnInit{

  predmeti: Predmet[] = [];
  private baseUrl = 'http://localhost:8080/api/studenti'

  constructor(private http: HttpClient, private authService : AuthService) {
    
  }

  ngOnInit(): void {

    this.http.get<Predmet[]>(`${this.baseUrl}/${this.authService.getUsername()}/predmeti`)
      .subscribe(data => {
        this.predmeti = data;
      });
  }

}

