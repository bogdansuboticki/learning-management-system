import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PredmetStudentiComponent } from './predmet-studenti.component';

describe('PredmetStudentiComponent', () => {
  let component: PredmetStudentiComponent;
  let fixture: ComponentFixture<PredmetStudentiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PredmetStudentiComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PredmetStudentiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
