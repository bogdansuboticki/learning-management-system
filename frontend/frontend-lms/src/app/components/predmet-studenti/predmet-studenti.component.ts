import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Predmet } from 'src/app/model/predmet';
import { Student } from 'src/app/model/student';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-predmet-studenti',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './predmet-studenti.component.html',
  styleUrl: './predmet-studenti.component.css'
})
export class PredmetStudentiComponent {

  nazivPredmeta: string | null = "";
  studenti: Student[] = [];
  private baseUrl = 'http://localhost:8080/api/nastavnici'
 
  constructor(private http: HttpClient, private authService : AuthService, private route : ActivatedRoute) {
   
  }
 
  ngOnInit(): void {
    
    this.route.paramMap.subscribe(params => {
      this.nazivPredmeta = params.get('naziv');
      console.log('KORISNICKO IME:', this.nazivPredmeta);
      this.http.get<Student[]>(`${this.baseUrl}/studenti-po-predmetu/${this.nazivPredmeta}`)
      .subscribe(data => {
        this.studenti = data;
      });
    });


  }

}
