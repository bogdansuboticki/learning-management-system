import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NastavnikIspitComponent } from './nastavnik-ispit.component';

describe('NastavnikIspitComponent', () => {
  let component: NastavnikIspitComponent;
  let fixture: ComponentFixture<NastavnikIspitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NastavnikIspitComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NastavnikIspitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
