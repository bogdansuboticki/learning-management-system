import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend-lms';
  showDropdown: boolean = false;
  constructor(private router: Router) {}

  navigateTo(route: string): void {
    this.router.navigateByUrl(route);
  }

  isLoggedIn(){
    return !!localStorage.getItem('currentUser');
  }

  logout(){
    localStorage.removeItem('currentUser');
    this.isLoggedIn();
  }

  toggleDropdown(): void {
    this.showDropdown = !this.showDropdown;
  }
}
