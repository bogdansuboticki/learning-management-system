export class NastavnikStudent {
    constructor(
        public ime: string,
        public brojIndeksa: string,
        public datumUpisa: string,
        public korisnickoIme: string
      ) {}
}