import { polozeniIspiti } from "./polozeniIspiti";

export class studentPodaci
{
    constructor(
        public ime : string,
        public jmbg : string,
        public brojIndeksa: string,
        public polozeni: polozeniIspiti[],
      ) {}
}
