export class GodinaStudija {
    id: number;
    godina: number;
  
    constructor(id: number, godina: number) {
      this.godina = godina;
      this.id = id;
    }
  }
  