import { Korisnik } from "./korisnik";

export class Nastavnik extends Korisnik {
    constructor(
      korisnickoIme: string,
      email: string,
      password: string,
      public nastavnikId: number,
    ) {
      super(korisnickoIme, email, password);
    }
  }