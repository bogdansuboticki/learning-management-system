import { Korisnik } from "./korisnik";

export class Administrator extends Korisnik {
    constructor(
      id: number,
      korisnickoIme: string,
      email: string,
      password: string,
      public isAdmin: boolean
    ) {
      super(korisnickoIme, email, password);
    }
  }