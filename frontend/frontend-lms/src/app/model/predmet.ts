export class Predmet
{
    constructor(
        public naziv : string,
        public espb : number,
        public obavezan: number,
        public brojPredavanja: number,
        public brojVezbi: number,
        public drugiObliciNastave: number,
        public istrazivackiRad: number,
        public ostaliCasovi: number
      ) {}
}
