export class Ispit{
    constructor(
        public naziv: string,
        public espb: number,
        public bodovi: number,
        public konacnaOcena: number
      ) {}
}