export class StudijskiProgram {
    naziv: string;
    fakultet: string;
    brojGodina: number;
  
    constructor(naziv: string, fakultet: string, brojGodina: number) {
      this.naziv = naziv;
      this.fakultet = fakultet;
      this.brojGodina = brojGodina;
    }
  }
  