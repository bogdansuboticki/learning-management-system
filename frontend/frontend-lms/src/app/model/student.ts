import { Korisnik } from "./korisnik";

export class Student extends Korisnik {
    constructor(
      public ime: string,
      korisnickoIme: string,
      email: string,
      public jmbg: string
    ) {
      super(korisnickoIme, email);
    }
  }