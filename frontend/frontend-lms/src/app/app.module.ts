import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AuthGuard } from './guards/auth.guard';
import { FakultetiComponent } from './components/fakulteti/fakulteti.component';
import { StudijskiProgramiComponent } from './components/studijski-programi/studijski-programi.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StudentDashboardComponent } from './components/student-dashboard/student-dashboard.component';
import { PredmetiComponent } from './components/predmeti/predmeti.component';
import { CommonModule } from '@angular/common';
import { NastavnikDashboardComponent } from './components/nastavnik-dashboard/nastavnik-dashboard.component';
import { ProfilComponent } from './components/profil/profil.component';
import { StudentskaSluzbaComponent } from './components/studentska-sluzba/studentska-sluzba.component';
import { UlogeComponent } from './components/uloge/uloge.component';
import { UredjivanjeUlogeComponent } from './components/uredjivanje-uloge/uredjivanje-uloge.component';
import { UpisComponent } from './components/upis/upis.component';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { SmeroviComponent } from './components/smerovi/smerovi.component';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { PredmetStudentiComponent } from './components/predmet-studenti/predmet-studenti.component';
import { NastavnikStudentiComponent } from './nastavnik-studenti/nastavnik-studenti.component';
import { NastavnikPredmetiComponent } from './nastavnik-predmeti/nastavnik-predmeti.component';
import { NastavnikStudentPodaciComponent } from './components/nastavnik-student-podaci/nastavnik-student-podaci.component';


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        AboutComponent,
        ContactComponent,
        LoginComponent,
        RegisterComponent,
        DashboardComponent,
        FakultetiComponent,
        StudijskiProgramiComponent,
        StudentDashboardComponent,
        NastavnikDashboardComponent,
        ProfilComponent,
        StudentskaSluzbaComponent,
        UlogeComponent,
        SmeroviComponent,
        NastavnikPredmetiComponent,

    ],
    providers: [AuthGuard, provideAnimationsAsync()],
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        PredmetiComponent,
        CommonModule,
        UredjivanjeUlogeComponent,
        UpisComponent,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatSnackBarModule,
        MatTableModule,
        MatToolbarModule,
        MatSidenavModule,
        MatListModule,        
        ConfirmationDialogComponent,
        MatDialogModule,
        PredmetStudentiComponent,
        NastavnikStudentiComponent,
        NastavnikStudentPodaciComponent,
        
    ]
})
export class AppModule { }
