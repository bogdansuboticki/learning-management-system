import { Component, NgModule, OnInit } from '@angular/core';
import { Student } from '../model/student';
import { HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'; // Import FormsModule
import { MatTableModule } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NastavnikStudent } from '../model/nastavnikStudent';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nastavnik-studenti',
  standalone: true,
  imports: [CommonModule, FormsModule, MatTableModule, MatFormFieldModule, MatSelectModule],
  templateUrl: './nastavnik-studenti.component.html',
  styleUrls: ['./nastavnik-studenti.component.css'] // Use styleUrls here
})
export class NastavnikStudentiComponent implements OnInit {

  private studentiUrl = 'http://localhost:8080/api/studenti/nastavnik/studenti';

  studenti: NastavnikStudent[] = [];
  filteredStudents: NastavnikStudent[] = [];
  filterTerm: string = '';
  selectedYear: number | null = null;
  constructor(private http: HttpClient, private snackBar: MatSnackBar, private router: Router) {
    
  }

  ngOnInit(): void {
 
    this.http.get<NastavnikStudent[]>(`${this.studentiUrl}`)
      .subscribe(data => {
        this.studenti = data;
        this.filteredStudents = this.studenti;
        console.log(this.studenti)
        console.log(this.filteredStudents[0].korisnickoIme)
      });
  
  }

  applyFilter() {
    this.filteredStudents = this.studenti.filter(student =>
      student.ime.toLowerCase().includes(this.filterTerm.toLowerCase()) ||
      student.brojIndeksa.toLowerCase().includes(this.filterTerm.toLowerCase()) ||
      student.datumUpisa.toLowerCase().includes(this.filterTerm.toLowerCase()) ||
      student.korisnickoIme.toLowerCase().includes(this.filterTerm.toLowerCase())
    );

    if (this.selectedYear) {
      this.filteredStudents = this.filteredStudents.filter(student => {
        const year = new Date(student.datumUpisa).getFullYear();
        return year === this.selectedYear;
      });
    
  }
}

get uniqueYears(): number[] {
  const years = this.studenti.map(student => new Date(student.datumUpisa).getFullYear());
  return Array.from(new Set(years));
}

clearFilter() {
  this.filterTerm = '';
  this.selectedYear = null;
  this.applyFilter(); 
}


onRowClick(korisnickoIme: String) {
  console.log("Ovde ide korisnicko ime: "  + korisnickoIme)
  this.router.navigate(['nastavnik' ,'nastavnik-student-podaci', korisnickoIme]);
}


}

