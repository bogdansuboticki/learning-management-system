import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AuthGuard } from './guards/auth.guard';
import { FakultetiComponent } from './components/fakulteti/fakulteti.component';
import { StudijskiProgramiComponent } from './components/studijski-programi/studijski-programi.component';
import { PredmetiComponent } from './components/predmeti/predmeti.component';
import { StudentDashboardComponent } from './components/student-dashboard/student-dashboard.component';
import { NastavnikDashboardComponent } from './components/nastavnik-dashboard/nastavnik-dashboard.component';
import { IspitiComponent } from './components/ispiti/ispiti.component';
import { StudentPrijavaispitaComponent } from './components/student-prijavaispita/student-prijavaispita.component';
import { ProfilComponent } from './components/profil/profil.component';
import { StudentAuthGuard } from './guards/student-auth-guard.guard';
import { NastavnikAuthGuard } from './guards/nastavnik-auth-guard.guard';
import { StudentskaSluzbaComponent } from './components/studentska-sluzba/studentska-sluzba.component';
import { studentskaSluzbaAuthGuard } from './guards/studentska-sluzba-auth.guard';
import { UlogeComponent } from './components/uloge/uloge.component';
import { UredjivanjeUlogeComponent } from './components/uredjivanje-uloge/uredjivanje-uloge.component';
import { UpisComponent } from './components/upis/upis.component';
import { SmeroviComponent } from './components/smerovi/smerovi.component';
import { NastavnikStudentiComponent } from './nastavnik-studenti/nastavnik-studenti.component';
import { NastavnikPredmetiComponent } from './nastavnik-predmeti/nastavnik-predmeti.component';
import { PredmetStudentiComponent } from './components/predmet-studenti/predmet-studenti.component';
import { NastavnikStudentPodaciComponent } from './components/nastavnik-student-podaci/nastavnik-student-podaci.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'login', component: LoginComponent },
  {path: 'fakulteti', component: FakultetiComponent},
  {path: 'studijski-programi', component: StudijskiProgramiComponent},
  { path: 'register', component: RegisterComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'student', component: StudentDashboardComponent, canActivate :[StudentAuthGuard],
  children: [      
    { path: 'predmeti', component: PredmetiComponent},
    { path: 'ispiti', component: IspitiComponent},
    { path: 'prijava-ispita', component:StudentPrijavaispitaComponent},
    {path: 'profil', component:ProfilComponent}]
  },
  { path: 'nastavnik', component: NastavnikDashboardComponent, canActivate: [NastavnikAuthGuard],
  children:[
    {path: 'nastavnik-predmeti', component:NastavnikPredmetiComponent},
    {path: 'nastavnik-studenti', component:NastavnikStudentiComponent},
    {path: 'predmet-studenti/:naziv', component:PredmetStudentiComponent},
    {path: 'nastavnik-student-podaci/:korisnickoIme', component:NastavnikStudentPodaciComponent}
  ]
},
  { path: 'studentska-sluzba', component: StudentskaSluzbaComponent, canActivate: [studentskaSluzbaAuthGuard],
    children: [ 
    { path: 'uloge', component: UlogeComponent},
    { path: 'uredi-ulogu/:korisnickoIme', component: UredjivanjeUlogeComponent },
    {path: 'upis', component: UpisComponent},
    {path: 'smerovi', component: SmeroviComponent} 
    ]
  },
  { path: '**', redirectTo: '' },
];
 
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
